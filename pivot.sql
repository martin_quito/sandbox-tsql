/*
"PIVOT and UNPIVOT are relational operators to chnage a a table-valued expression into another table. PIVOT rotates a table-valued expression by turning the unique values from one column in the expression into multiple columns in the output, and performs aggregations where they are required on any remaining column values that are wanted in the final output."

"UNPIVOT performs the opposite operation to PIVOT by rotating columns of a table-valued expression into column values"

*/

-- =====================
-- started example
-- =====================
DECLARE @DailyIncome TABLE (NickName NVARCHAR(MAX),  aDay NVARCHAR(MAX), Income INT)
insert into @DailyIncome values ('SPIKE', 'FRI', 100)
insert into @DailyIncome values ('SPIKE', 'MON', 300)
insert into @DailyIncome values ('FREDS', 'SUN', 400)
insert into @DailyIncome values ('SPIKE', 'WED', 500)
insert into @DailyIncome values ('SPIKE', 'TUE', 200)
insert into @DailyIncome values ('JOHNS', 'WED', 900)
insert into @DailyIncome values ('SPIKE', 'FRI', 100)
insert into @DailyIncome values ('JOHNS', 'MON', 300)
insert into @DailyIncome values ('SPIKE', 'SUN', 400)
insert into @DailyIncome values ('JOHNS', 'FRI', 300)
insert into @DailyIncome values ('FREDS', 'TUE', 500)
insert into @DailyIncome values ('FREDS', 'TUE', 200)
insert into @DailyIncome values ('SPIKE', 'MON', 900)
insert into @DailyIncome values ('FREDS', 'FRI', 900)
insert into @DailyIncome values ('FREDS', 'MON', 500)
insert into @DailyIncome values ('FREDS', 'MON', 2000)
insert into @DailyIncome values ('JOHNS', 'SUN', 600)
insert into @DailyIncome values ('SPIKE', 'FRI', 300)
insert into @DailyIncome values ('SPIKE', 'WED', 500)
insert into @DailyIncome values ('SPIKE', 'FRI', 300)
insert into @DailyIncome values ('JOHNS', 'THU', 800)
insert into @DailyIncome values ('JOHNS', 'SAT', 800)
insert into @DailyIncome values ('SPIKE', 'TUE', 100)
insert into @DailyIncome values ('SPIKE', 'THU', 300)
insert into @DailyIncome values ('FREDS', 'WED', 500)
insert into @DailyIncome values ('SPIKE', 'SAT', 100)
insert into @DailyIncome values ('FREDS', 'SAT', 500)
insert into @DailyIncome values ('FREDS', 'THU', 800)
insert into @DailyIncome values ('JOHNS', 'TUE', 600)

-- using groups
SELECT NickName, aDay, AVG(Income)
FROM @DailyIncome
GROUP BY NickName, aDay
ORDER BY NickName, aDay

-- using pivot
SELECT *
FROM @DailyIncome
PIVOT (
	AVG (Income) 
FOR aDay 
	IN ([Mon], [Tue], [Wed], [FRI], [Sat], [Sun])
) AS AveIncomePerDay

	-- . in the IN clause, if you put an non-existen item, it will work, but NULLs will be listed on

/*
SELECT <non-pivoted column>,  
    [first pivoted column] AS <column name>,  
    [second pivoted column] AS <column name>,  
    ...  
    [last pivoted column] AS <column name>  
FROM  
    (<SELECT query that produces the data>)   AS <alias for the source query>  
PIVOT  
(  
    <aggregation function>(<column being aggregated>)  
FOR   
	[<column that contains the values that will become column headers>]   
		IN ( [first pivoted column], [second pivoted column],  
		... [last pivoted column])  
	) AS <alias for the pivot table>  

<optional ORDER BY clause>;  

- the non-pivoted column can be done multiple times
- the alias is required*
*/

-- ==============================
-- pivoted column does not exist
-- ==============================
DECLARE @birthdays TABLE (Name NVARCHAR(MAX), aMonth NVARCHAR(MAX), Yes BIT)
INSERT INTO @birthdays VALUES
  ('Martin', 'DEC', 1)
, ('Martin', 'DEC', 1)
, ('Miriam', 'OCT', 1)
, ('Nefi', 'APRL', 1)
, ('Moises', 'JUL', 1)
, ('Catherine', 'MAY', 1)

SELECT *
FROM @birthdays
PIVOT ( 
	COUNT(Yes)
	FOR aMonth IN ([JAN], [FEB], [APRL], [MAY], [JUL], [OCT], [DEC]) 
) As pvt
go

-- ==============================
-- how data aggregates
-- ==============================
DECLARE @birthdays TABLE 
(
	FirstName NVARCHAR(MAX), 
	LastName NVARCHAR(MAX),
	Age int,
	SaleMonth NVARCHAR(MAX), 
	Yes BIT
)
INSERT INTO @birthdays VALUES
  ('Martin', 'Quito1.5', 24, 'DEC', 1)
, ('Martin', 'Quito1.5', 28, 'DEC', 1)
, ('Martin', 'Quito1.5', 28, 'DEC', 1)
, ('Miriam', 'Quito2', 25, 'OCT', 1)
, ('Nefi', 'Quito3', 27, 'APRL', 1)
, ('Moises', 'Quito4', 28, 'JUL', 1)
, ('Catherine', 'Quito5', 29, 'MAY', 1)

select FirstName, LastName, Age, SaleMonth, count(*)
from @birthdays
group by FirstName, LastName, Age, SaleMonth

SELECT *
FROM @birthdays
PIVOT ( 
	COUNT(Yes) -- aggrevated value
	FOR SaleMonth IN ([JAN], [FEB], [APRL], [MAY], [JUL], [OCT], [DEC])
) As pvt

/*
ALL the columns, with the exception of the aggregated column, get grouped as if you were doing a group by (see above).
Then the column designated to be a pivoted coumn (SaleMonth) will be pivoted and their values will be the aggregated result of count(*). Then the columns with the exception of the aggregated column and the pivoted column will get grouped.
*/


select FirstName, LastName, SaleMonth, Age, count(*)
from @birthdays
group by FirstName, LastName, SaleMonth, Age

SELECT *
FROM @birthdays
PIVOT ( 
	COUNT(Yes) -- aggrevated value
	FOR Age IN ([24], [28], [25], [27], [29])
) As pvt

/*
In this example, if you look at the group, you will notice how 'Martin' 'Quito 1.5' will become one row with 24 and 28 columsn and a value in each of them
*/

-- ====================================
-- using max 
-- ==============================
DECLARE @EmailCount TABLE (Name NVARCHAR(MAX), aDay NVARCHAR(MAX), NumberOfEmails INT)
INSERT INTO @EmailCount VALUES 
('Martin', 'MON', 10)
, ('Martin', 'MON', 20)
, ('Martin', 'MON', 0)
, ('Martin', 'MON', 1)
, ('Martin', 'TUE', 2)
, ('Martin', 'TUE', 100)
, ('Nefi', 'MON', 23)
, ('Nefi', 'MON', 50)
, ('Nefi', 'MON', 23)
, ('Nefi', 'THR', 44)
, ('Nefi', 'THR', 2)
, ('Miriam', 'SAT', 23)
, ('Miriam', 'MON', 3)
, ('Miriam' ,'MON', 23)

SELECT *
FROm @EmailCount
PIVOT (
	MAX(NumberOfEmails) -- the number to be grouped
	FOR aDay IN ([Mon], [TUE], [THR], [SAT])-- the "X"! 
) AS pvt

 -- =======================================================

 -- =======================================================
 -- the versions of each proposal
;WITH data AS
(
	SELECT DefaultDisplayType AS FieldType
	, IsRequired AS IsRequired
	, COUNT(*) AS aCount
	FROM MetaSelectedField
	GROUP BY DefaultDisplayType,IsRequired
)	SELECT *
	FROM data d
	PIVOT (
		SUM(aCount)
		FOR IsRequired IN ([1], [0])
	) AS pvt
		
	 -- the correct values must be supplijed int FOR () secion 

-- ========================================================

-- ========================================================
;WITH Data AS
(
	SELECT c.StatusAliasId
	, cr.RequisiteTypeId
	, COUNT(c.Id) AS NumberOfCourses
	FROM Course c
		INNER JOIN CourseRequisite cr ON c.Id = cr.CourseId
	GROUP BY c.StatusAliasId, cr.RequisiteTypeId
), SourceData AS
(
	SELECT sa.Title StatusAlias
	, CASE WHEN rt.Title IS NULL THEN 'Fail' ELSE rt.Title  END AS Requisite
	, d.NumberOfCourses
	FROM Data d
		LEFT JOIN StatusAlias sa ON d.StatusAliasId = sa.Id
		LEFT JOIN RequisiteType rt ON d.RequisiteTypeId = rt.Id	
)	
	SELECT *
	FROM SourceData d
	PIVOT (
		SUM(d.NumberOfCourses)
		FOR d.Requisite IN (Prerequisite, Corequisite, Recommendation, Fail) 
	) AS pvt


-- ========================================================
-- alias vs 2 tier
-- ========================================================
;WITH ProgramWithTiers AS
(
	SELECT p.StatusAliasId, p.Tier2_OrganizationEntityId, COUNT(p.Id) AS Programs
	FROM Program p
	GROUP BY p.StatusAliasId, p.Tier2_OrganizationEntityId
)
, ProgramWithTiersByName AS
(
	SELECT sa.Title + '-' + CAST(sa.Active AS NVARCHAR(MAX)) AS aStatus
	, oe.Title AS Departs
	, pwt.Programs
	FROM ProgramWithTiers pwt
		INNER JOIN StatusAlias sa ON pwt.StatusAliasId = sa.Id
		INNER JOIN OrganizationEntity oe ON pwt.Tier2_OrganizationEntityId= oe.Id
)	SELECT *
	FROM ProgramWithTiersByName
	PIVOT (
		SUM(Programs)
		FOR Departs IN ([Accounting]
,[Administration of Justice]
,[Athletics]
,[Automotive Technology]
,[Basic Skills-English]
,[Basic Skills-Reading]
,[Basic Skills-Writing]
,[Biology]
,[Building Information Modeling]
,[Computer Information Technology]
,[Corrections]
,[Economics]
,[Engineering]
,[English]
,[English as a Second Language]
,[Law Enforcement]
,[Library Studies]
,[Mathematics]
,[Physical Science]
,[Women's Studies])) AS pvt



;WITH ProgramWithTiers AS
(
	SELECT p.StatusAliasId, p.Tier2_OrganizationEntityId, COUNT(p.Id) AS Programs
	FROM Program p
	GROUP BY p.StatusAliasId, p.Tier2_OrganizationEntityId
)
, ProgramWithTiersByName AS
(
	SELECT sa.Title + '-' + CAST(sa.Active AS NVARCHAR(MAX)) AS aStatus
	, oe.Title AS Departs
	, pwt.Programs
	FROM ProgramWithTiers pwt
		INNER JOIN StatusAlias sa ON pwt.StatusAliasId = sa.Id
		INNER JOIN OrganizationEntity oe ON pwt.Tier2_OrganizationEntityId= oe.Id
)	SELECT *
	FROM ProgramWithTiersByName
	PIVOT (
		SUM(Programs)
		FOR aStatus IN ([Active-1], [Draft-1], [Historical-1], [In Review-1])) AS pvt

	-- . the formual is PIVOT ( AGGR_CLAUSE[what is counted] FOR [THE X - axis or Y-axis] IN [the values on the selected axis])

-- =============================

-- =============================
DECLARE @RawData TABLE (ActiveCourseProp NVARCHAR(MAX), UpdateCourseProp NVARCHAR(MAX), Step NVARCHAR(MAX), Actions NVARCHAR(MAX))
INSERT INTO @RawData VALUES
  (1, 1, 'Faculty', 'Launch')
, (1, 2, 'Department Chair', 'Approve, Decline')
, (2, 3, 'Senate Subcommittee', 'Approve, Reviewed')

SELECT  ProposalTypes, Step, Levels, Actions -- selects the new column names
FROM (  SELECT UpdateCourseProp, Step, ActiveCourseProp, Actions
		FROM @RawData ) p	-- loads the data
UNPIVOT
	(Levels FOR ProposalTypes IN
		(ActiveCourseProp, UpdateCourseProp) -- select here what will change
) AS unpvt
ORDER BY ProposalType


-- =============================

-- =============================
select *
from (
	values
	(1023, 'Bit01'),
	(1023, 'Bit02'),
	(1023, 'Bit02'),
	(1023, 'Bit03'),
	(1023, 'Bit04'),
	(1023, null),
	(1024, 'Bit01'),
	(1024, 'Bit02'),
	(1025, 'Bit01')
) Source (CourseId, ValuesChecked)
pivot (
	count(Source.ValuesChecked)
	for Source.ValuesChecked in ([Bit01], [Bit02], [Bit03], [Bit04])
) p1

-- =============================
-- unpivot
-- =============================
select up1.CourseId, up1.ColumnName, up1.DataIdentifier
from (
	values
	(1023, 1, 0, 1),
	(1024, 1, 1, 1),
	(1025, 0, 0, 1)
) Source (CourseId, Bit01, Bit02, Bit03)
unpivot (
	-- this is another column name I'm giving for a new column that will hold the values that CURRENTLY exist 
	-- under the columns being rotated
	[ColumnName] 
	-- I have to give a name to a new column with column valuies that I'm rotating
	for DataIdentifier in ([Bit01], [Bit02], [Bit03])
) as up1