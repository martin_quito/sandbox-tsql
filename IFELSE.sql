IF EXISTS (SELECT 1 FROM StatusAlias WHERE StatusBaseId = 10)   
	SELECT 'FIRST'
ELSE IF EXISTS (SELECT * FROM Program WHERE Id = 0) 
		SELECT 'SECOND1'
ELSE 
	SELECT 'THIRD1'

/*
- if or else, statement block needs BEGIN and END. If one line, sql statement, then no BEGIN or END
- else has no boolean expression
*/