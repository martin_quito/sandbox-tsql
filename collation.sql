/*
What is sql collation?
	- provides rules when using character types for querying and storing
		- sort
		- compare data
		- casing
	- sql server sets a default collation
		e.g 'Latin1_General_CI_AI'
	- users can change the collation on database level but not sql level, but of course, with sql installation, the default sql collation can be set
	- to get a list of collations supported by the installed version of SQL server, you can call 'sys.fn_helpcollations()'
	- sql server allows colaltions to be configured at different levels of the database engine, but by default, every level will inherit the collation settings from the parent leve
		- sql server collation
		- database collation
		- column collation
		- expression collation

Resource
- https://www.sqlshack.com/sql-server-collation-introduction-with-collate-sql-casting/
- https://docs.microsoft.com/en-us/sql/relational-databases/collations/collation-and-unicode-support?view=sql-server-ver15
*/

-- ===========
-- get supported collations
-- ===========
select *
from sys.fn_HelpCollations()

-- =============
-- Sql server (at work) collation
-- =============
-- SQL_Latin1_General_CP1_CI_AS
	-- latin1 -> makes the server treat strings using charset 1, basic
   

select *
from [User]
order by FirstName

update [User]
set FirstName = 'albert'
where id = 558