/*
Rules
- updates the value of a property in a json string and returns the update json string
*/

-- =================
-- basic operations
-- =================
declare @info nvarchar(max) = '{"name":"John","skills":["C#","SQL"]}';

select json_modify(@info, '$.name', 'Mike')
, json_modify(@info, '$.name', null)
, json_modify(@info, 'append $.skills', 'Azure')
, json_modify(@info, '$.newvalue', 'hello')


---- =================
---- rename a key
---- =================
--declare @product nvarchar(100) = '{"price":49.99}'

--select json_modify(
--	json_modify(@product, '$.Price', cast(json_value(@product, '$.price') as numeric(4,2))),
--	'$.price',
--	null
--)
--, json_modify(@product, '$.Price', cast(json_value(@product, '$.price') as numeric(4,2)))

-- =================
-- modify a json object
-- =================
declare @info nvarchar(max) = '{"name":"John","skills":["C#","SQL"]}';

select 
-- it treats the new value as plain text and therefore the double qotes are escaped
json_modify(@info, '$.skills', '["C#","T-SQL","Azure"]'),

-- json_query returns a properly formatted json because the value gets escaped
json_modify(@info, '$.skills', json_query('["C#","T-SQL","Azure"]'))