-- ==================
-- ISJSON
-- ==================
declare @param nvarchar(MAX) = '{"name": "Martin"}';
if (isjson(@param) > 0)
begin;
	select 'it is json';
end;