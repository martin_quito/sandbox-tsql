/*
Rules
- JSON functions allows you to combine NoSQL and relational concetps in the same database
	- loading json
	- read json
	- modify json
	- transform arrays of json objects into table format
	- run any tsql on converted json objects
	- change schema
- use "OPENJSON"
	- transforms JSON text to table
- use "FOR JSON"
	- transforms result set as JSON text

- JSON bult-in functions
	ISJSON	
		- verify that text is formatted as JSON
	JSON_VALUE
		- extract value from JSON text
	JSON_QUERY
		- extract JSON fragment from JSON text
	JSON_MODIFY
		- update, delete, or add properties in JSON text

- JSON Path Expressions	
	We will have to provide a path expression in the following functions
		OPENJSON
		JSON_VALUE
		JSON_QUERY
		JSON_MODIFY

*/


