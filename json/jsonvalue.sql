/*
Rules
- Extracts a scalar value from a JSON string.
- If you want to extra an object or array, use JSON_QUERY
- syntax
	json_value (expression, path)
- lax is the default path mode 
*/

---- ==================
---- exmaple 1
---- ==================
--declare @jsonInfo nvarchar(max)
--set @jsonInfo=N'{  
--     "info":{    
--       "type":1,  
--       "address":{    
--         "town":"Bristol",  
--         "county":"Avon",  
--         "country":"England"  
--       },  
--       "tags":["Sport", "Water polo"]  
--    },  
--    "type":"Basic"  
-- }'  


--select json_value(@jsonInfo, '$.info.type')
--, json_value(@jsonInfo, '$.info.address.town')
--, json_value(@jsonInfo, '$.info.address')

-- ==================
-- where clause
-- ==================
declare @colors nvarchar(max) = '{
  "colors": [
    {
      "color": "black",
      "category": "hue",
      "type": "primary",
      "code": {
        "rgba": [255,255,255,1],
        "hex": "#000"
      }
    },
    {
      "color": "white",
      "category": "value",
      "code": {
        "rgba": [0,0,0,1],
        "hex": "#FFF"
      }
    },
    {
      "color": "red",
      "category": "hue",
      "type": "primary",
      "code": {
        "rgba": [255,0,0,1],
        "hex": "#FF0"
      }
    },
    {
      "color": "blue",
      "category": "hue",
      "type": "primary",
      "code": {
        "rgba": [0,0,255,1],
        "hex": "#00F"
      }
    },
    {
      "color": "yellow",
      "category": "hue",
      "type": "primary",
      "code": {
        "rgba": [255,255,0,1],
        "hex": "#FF0"
      }
    },
    {
      "color": "green",
      "category": "hue",
      "type": "secondary",
      "code": {
        "rgba": [0,255,0,1],
        "hex": "#0F0"
      }
    },
  ]
}'


select json_value(@colors, '$.colors[0].color')