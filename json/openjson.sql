/*
Rules:
- takes a single JSON object or collection of objects and transforms them into one or more dows
- by default, it returns 
	- from a json object, key value pairs that it finds at the first level
	- from an array, all eleemtnst of the array with their indexes
*/

-- ==============
-- default, one single json object
-- ==============
declare @json nvarchar(MAX)

set @json='{"name":"John","surname":"Doe","age":45,"skills":["SQL","C#","MVC"]}';

select *
from openjson(@json);

/*
key		value				type
name	John				1
surname	Doe					1
age		45					2
skills	["SQL","C#","MVC"]	4
*/

-- ==============
-- default, json array
-- ==============
declare @json1 nvarchar(max) = '
	[1,2,3,4,{"name": "martin"}]
'

select *
from openjson(@json1)

/*
key	value				type
0	1					2
1	2					2
2	3					2
3	4					2
4	{"name": "martin"}	5
*/

-- . it only parses the first level
-- . the value of the object is a valid json

-- ================
-- path is optional to reference an object or an array
-- ================
declare @json nvarchar(max) = '
	{
		"order": {
			"user": {
				"name": "martin"
			}
		}
	}
';

select *
from openjson(@json, '$.order.user')



-- ==============
-- using 'with caluse' explicit structure - json array
-- ==============
declare @json nvarchar(max) =   N'[  
       {  
         "Order": {  
           "Number":"SO43659",  
           "Date":"2011-05-31T00:00:00"  
         },  
         "AccountNumber":"AW29825",  
         "Item": {  
           "Price":2024.9940,  
           "Quantity":1  
         }  
       },  
       {  
         "Order": {  
           "Number":"SO43661",  
           "Date":"2011-06-01T00:00:00"  
         },  
         "AccountNumber":"AW73565",  
         "Item": {  
           "Price":2024.9940,  
           "Quantity":3  
         }  
      }  
 ]'

 select *
 from openjson(@json)
 with (
	Number varchar(200) '$.Order.Number',
	Date datetime '$.Order.Date',
	[Custom] varchar(200) '$.AccountNumber',
	Quantity int '$.Item.Quantity', -- the '$...' is the path that specifies the property in the json to return in this specified column
	AccountNumber nvarchar(max), -- this is the default, this name is used to match a property in the json

	-- if you use 'as json', the type must be 'nvarchar(max)'
	-- 'as json' is used to specify that the referenced property contains an inner json objet or array
	-- this returns the json object for order object
	-- form documentation 'If you specify AS JSON for a column, the function returns a JSON fragment from the specified JSON property on the specified path'
	[Order] nvarchar(max) as json,

	-- this comes out null, why?
	-- documentation says it returns a scalar value, int, string, true, or false. It should return a string right? But is not, it returns null
	-- my conclusion is that if the path represents an object or an array, and have 'no json', then it returns null
	[Order] nvarchar(max) '$.Order'

	-- throw error "Conversion failed when converting the nvarchar value 'AW29825' to data type int."
	--AccountNumber int	
 )


-- ==============
-- open json to one array with one element and returns each row as json
-- ==============
declare @json nvarchar(max) =   N'[  
{"caption":"Subject Code","sorting":"Ascending","sortIndex":-1,"expr":{"typeName":"ENTATTR","id":"SubjectId_Subject_Course.SubjectSubjectCode"}}
 ]';


select *
from openjson(@json)
with (
	Number nvarchar(max) '$' AS JSON 
)

-- ==================
-- explicit structure - json object
-- ==================
declare @data nvarchar(max) = '{
  "colors": [
    {
      "color": "black",
      "category": "hue",
      "type": "primary",
      "code": {
        "rgba": [255,255,255,1],
        "hex": "#000"
      }
    },
    {
      "color": "white",
      "category": "value",
      "code": {
        "rgba": [0,0,0,1],
        "hex": "#FFF"
      }
    },
    {
      "color": "red",
      "category": "hue",
      "type": "primary",
      "code": {
        "rgba": [255,0,0,1],
        "hex": "#FF0"
      }
    },
    {
      "color": "blue",
      "category": "hue",
      "type": "primary",
      "code": {
        "rgba": [0,0,255,1],
        "hex": "#00F"
      }
    },
    {
      "color": "yellow",
      "category": "hue",
      "type": "primary",
      "code": {
        "rgba": [255,255,0,1],
        "hex": "#FF0"
      }
    },
    {
      "color": "green",
      "category": "hue",
      "type": "secondary",
      "code": {
        "rgba": [0,255,0,1],
        "hex": "#0F0"
      }
    }
  ]
}'

select *
from openjson(json_query(@data, '$.colors'))
with (
	ColorName nvarchar(max) '$.color',
	Category nvarchar(max) '$.category',
	Hex nvarchar(max) '$.code.hex'
)

select *
from openjson(@data)
with (
	Colors nvarchar(max) '$.colors' AS JSON
)

select *
from openjson(@data)

select *
from openjson(@data)
with (
	Colors nvarchar(max) '$.colors'
)

select *
from openjson(@data)
with (
	colors nvarchar(max) as json
)

select *
from openjson(@data, '$.colors')
with (
	ColorName nvarchar(max) '$.color'
)
