/*
- when you use 'FOR JSON' you can let the select statement determine the structure (FOR JSON AUTO) or you can specify the structure using (FOR JSON PATH)
- "PATH mode lets you create wrapper objects and nest complex properties. The results are formatted as an array of JSON objects."
- FOR JSON AUTO or FOR JSON PATH, there are more options to control the output
	- ROOT
	- INCLUDE_NULL_VALUES
	- WITHOUT_ARRAY_WRAPPER
*/

-- =======================
-- specifying the structure using FOR JSON PATH - returning an array
-- =======================
declare @data table (Id int, FirstName nvarchar(max), LastName nvarchar(max), Age int);
insert into @data
values
(1, 'Martin', 'Quito', 31),
(2, 'Nefi', 'Quito', 33),
(3, 'Miriam', 'Quito', 35),
(4, 'Moises', 'Quito', 28)

select Id
, FirstName as [User.First]
, LastName as [User.Last]
from @data
for json path
/*
[
  {
    "Id": 1,
    "User": {
      "First": "Martin",
      "Last": "Quito"
    }
  },
  {
    "Id": 2,
    "User": {
      "First": "Nefi",
      "Last": "Quito"
    }
  },
  {
    "Id": 3,
    "User": {
      "First": "Miriam",
      "Last": "Quito"
    }
  },
  {
    "Id": 4,
    "User": {
      "First": "Moises",
      "Last": "Quito"
    }
  }
]
*/
go

-- =======================
-- specifying the structure using FOR JSON PATH - setting a root
-- =======================
declare @data table (Id int, FirstName nvarchar(max), LastName nvarchar(max), Age int);
insert into @data
values
(1, 'Martin', 'Quito', 31),
(2, 'Nefi', 'Quito', 33),
(3, 'Miriam', 'Quito', 35),
(4, 'Moises', 'Quito', 28)

select Id
, FirstName as [User.First]
, LastName as [User.Last]
from @data
for json path, root('Accounts')

/*

{
  "Accounts": [
    {
      "Id": 1,
      "User": {
        "First": "Martin",
        "Last": "Quito"
      }
    },
    {
      "Id": 2,
      "User": {
        "First": "Nefi",
        "Last": "Quito"
      }
    },
    {
      "Id": 3,
      "User": {
        "First": "Miriam",
        "Last": "Quito"
      }
    },
    {
      "Id": 4,
      "User": {
        "First": "Moises",
        "Last": "Quito"
      }
    }
  ]
}
*/
go
-- ======================
-- using FOR JSON AUTO to let the select statement strucutre the output
-- ======================
declare @data table (Id int, FirstName nvarchar(max), LastName nvarchar(max), Age int);
insert into @data
values
(1, 'Martin', 'Quito', 31),
(2, 'Nefi', 'Quito', 33),
(3, 'Miriam', 'Quito', 35),
(4, 'Moises', 'Quito', 28)

select *
from @data
for json auto

/*
[
  {
    "Id": 1,
    "FirstName": "Martin",
    "LastName": "Quito",
    "Age": 31
  },
  {
    "Id": 2,
    "FirstName": "Nefi",
    "LastName": "Quito",
    "Age": 33
  },
  {
    "Id": 3,
    "FirstName": "Miriam",
    "LastName": "Quito",
    "Age": 35
  },
  {
    "Id": 4,
    "FirstName": "Moises",
    "LastName": "Quito",
    "Age": 28
  }
]
*/