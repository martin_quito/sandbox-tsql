/*
Rules
- extracts an object or an array from a json string
- returns a json fragment
	- returns null or error if the value is not an object or an array
*/

-- ==================
-- exmaple 1
-- ==================
declare @jsonInfo nvarchar(max)
set @jsonInfo=N'{  
     "info":{    
       "type":1,  
       "address":{    
         "town":"Bristol",  
         "county":"Avon",  
         "country":"England"  
       },  
       "tags":["Sport", "Water polo"]  
    },  
    "type":"Basic"  
 }'  


select json_query(@jsonInfo, '$.info.address')
/*
Returns
{    
    "town":"Bristol",  
    "county":"Avon",  
    "country":"England"  
}
*/

