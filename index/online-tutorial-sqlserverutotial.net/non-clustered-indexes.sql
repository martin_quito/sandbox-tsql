/*
Learning
	- Attributes
		- is similar to the clustered index with the exception that it copies the index key values from the original table to another place and sorts it out in the new place. A clustered index sorts out the data in the actual, physical table. But not a non-clustered index. So this non-clustered index copies the index key values to another places, sorts it out there, and also contains a "row pointers" to the data rows that contain the key values! If the table has a clustered index, then the row pointer is the clustered index key; if not, then the row pointer will point to the row of the table (that is when the table is in the heap)
		- a table can contain only one clustered index and multiple non-clustered indexes
		- the ordere of the columns in the index definition is v ery important. It is a good practive to place the columsn that you often use to query data at the beginning of the columsn list of the index
			- need to learn more of the why!


*/
--use BikeStores;

-- ====================
-- non clustered on one column
-- ====================
select customer_id, city
from sales.customers
where city = 'Atwater'

CREATE INDEX ix_customers_city
ON sales.customers(city);
	-- DROP INDEX ix_customers_city on sales.customers

	

-- ====================
-- non clustered on multiple columns
-- ====================
select 
	first_name,
	last_name
from
	sales.customers
where
	last_name = 'Berg' and
	first_name = 'Monika'
-- this does a index scan "Clustered Index Scan (Clustered)"

CREATE INDEX ix_customers_name 
ON sales.customers(last_name, first_name);
	-- DROP INDEX ix_customers_name on sales.customers
-- if you run the query again above, it will do a "Index Seek (NonClustered)" search

SELECT 
    customer_id, 
    first_name, 
    last_name
FROM 
    sales.customers
WHERE 
    last_name = 'Albert';
-- this does a "Index Seek (NonClustered)"

-- vs

SELECT 
    customer_id, 
    first_name, 
    last_name
FROM 
    sales.customers
WHERE 
    first_name = 'Adam';
-- this does a "Index Scan (NonClustered)"

-- it is a good practice to place the columsn that I often you to query data at the beginning of the column
-- the previous query where it does a "...first_name = 'Adam'" it is better in performance than running it without a nonclustered index. Even though the query does a scan, it does it in a non-clustered index instead of a clustered index scan

-- ======================
-- inner joins
-- ======================
select customer_id, city
from sales.customers
where city = 'Atwater'

CREATE INDEX ix_customers_city
ON sales.customers(city);
	-- DROP INDEX ix_customers_city on sales.customers

select c.customer_id
, c.city
from sales.customers c
	inner join sales.orders o on c.customer_id = o.customer_id
where c.city = 'Atwater'
