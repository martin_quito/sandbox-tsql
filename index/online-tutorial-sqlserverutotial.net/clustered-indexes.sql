/*
Learning
- a table can only have one CLUSTERED INDEX. It makes the table be in a sorted structured which means is ordered in one way!
- the CLUSTERED INDEX can be applied to more than one column

*/

-- ======================
-- basics
-- ======================
create table Production.Parts(
    part_id   int not null,
    part_name varchar(100)
);

insert into Production.Parts(part_id, part_name)
values
(1, 'Frame'),
(2, 'Head Tube'),
(3, 'Handlebar Grip'),
(4, 'Shock Absorber'),
(5, 'Fork')

-- what is going here. The table "Production.Parts" does not have a "primary key", therefore SQL Server stores its rows in an ordered structure called a "heap"
-- when you query the data from the "Production.Parts" table, the "query optimizer" needs to scan the whole table to locate the correct one.

select part_id
, part_name
from Production.Parts
where part_id = 5

-- To show the "query plan", click on the "Display Estimated Execution Plan"

-- to resolve this issue, we can add an index
-- in this case we will add a clustered index
-- a clustered index stores data rows in a "sorted structured" based on key/values.
-- each table has only ONE clustered index because the data can be only sorted in one order!!
-- a table that has a clustered index is called an "clustered table"
-- the "stored structure" is a "B-Tree" which enables searches, inserts, updates, and deletes in logarithmic amortized time

-- when you create a primary key, SQL server automatically creates a corresponding clustered index on columns includede in the primary keyt
CREATE TABLE production.part_prices(
    part_id int,
    valid_from date,
    price decimal(18,4) not null,
    PRIMARY KEY(part_id, valid_from) 
);
-- . note that SQL server generated a random Primary key name
-- . index, constraint, primary key, are related terms
-- . what happened here is that a clustered index was added on two columns (part_id, valid_form)

-- ======================
-- do a CREATE CLUSTERED INDEX
-- ======================
-- if a table does not have a primary key (which automalliy adds the clustered index), you can use the CREATE CLUSTERED INDEX
CREATE CLUSTERED INDEX ix_parts_id
ON production.parts (part_id)

-- if we do the previous search
select *
from Production.Parts
where part_id = 3

-- open the estimated exuection plan
-- you will notice that it will not scan the whole table
