/*
- ensures the index key columns do not container any duplicate values
- can be applied to one or many columns
- can be clustered or non-clustered
- a CREATE UNIQUE INDEX ... creates a non-clustered index (I think this happens when a clustered index already exists, maybe if a clustered index does not exist, it will create a clustered unique index)
*/