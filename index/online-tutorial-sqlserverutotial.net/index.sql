/*
- indexes are special data sctureus with tables or views that help up speed up the query
- sql server provides two types of indexes
	- clustered index
	- non-clustered index
 

*/

use ccsf;


CREATE NONCLUSTERED INDEX [IX_NotificationActionReminder_Meta1] ON [dbo].[NotificationActionReminder] (ProposalId, StepId, UserId)
INCLUDE (NotificationQueueId, EndDate)

create nonclustered index idx_test1 on [dbo].[NotificationActionReminder] (ProposalId, StepId, UserId)
include (NotificationQueueId, EndDate)
-- drop index idx_test1 on [dbo].NotificationActionReminder
-- drop index [IX_NotificationActionReminder_Meta1] on [dbo].NotificationActionReminder

create nonclustered index idx_test2 on [dbo].[NotificationActionReminder] (ProposalId)
--include (EndDate, NotificationQueueId)
-- drop index idx_test2 on [dbo].NotificationActionReminder

create nonclustered index idx_test3 on [dbo].[NotificationActionReminder] (StepId)
-- drop index idx_test3 on [dbo].NotificationActionReminder

create nonclustered index idx_test4 on [dbo].[NotificationActionReminder] (UserId)
-- drop index idx_test4 on [dbo].NotificationActionReminder




select nah2.Id
from NotificationActionReminder nah2
where nah2.ProposalId = 3501
and nah2.StepId = 8488
and nah2.UserId = 43998





select nah2.Id, nah2.EndDate
from NotificationActionReminder nah2
where nah2.ProposalId = 3501
and nah2.StepId = 8488
and nah2.UserId = 43998

select psah.Id
, psah.StepId
from ProcessStepActionHistory psah
	--inner join ProcessLevelActionHistory plah on psah.ProcessLevelActionHistoryId = plah.Id
where psah.UserId = 42208


select *
from StatusAlias sa
where sa.StatusBaseId = 1

select *
from NotificationActionReminder

delete from NotificationActionReminder

select *
from NotificationQueue

--insert into NotificationActionReminder
drop table if exists #temp
create table #temp 
(
	Id int not null identity primary key,
	ProposalId int,
	CourseId int,
	ProgramId int
)

insert into #temp (ProposalId, CourseId, ProgramId)
select p.Id as ProposalId
, c.Id as CourseId
, pr.Id as ProgramId
from Proposal p
	left join Course c on p.Id = c.ProposalId
	left join Program pr on p.Id = pr.ProposalId
where c.Id is not null
or pr.Id is not null



--create nonclustered index idx_test200 on [dbo].NotificationQueue (CourseId, ProgramId)  include (Id)
--drop index idx_test200 on [dbo].NotificationQueue

--create nonclustered index idx_test201 on [dbo].NotificationQueue (CourseId) include (ProgramId, Id)
--drop index idx_test201 on [dbo].NotificationQueue

--create nonclustered index idx_test202 on [dbo].NotificationQueue (ProgramId)  include (CourseId, Id)
--drop index idx_test202 on [dbo].NotificationQueue

delete from NotificationActionReminder

select count(*)
from NotificationActionReminder


insert into NotificationActionReminder
select t.ProposalId
, ca.StepId
, ca.UserId
, ca2.Id
, current_timestamp
--, t.Id
from #temp t
	cross apply (
		select plah.Id, psah.StepId
		, psah.UserId
		from ProcessLevelActionHistory plah 
			inner join ProcessStepActionHistory psah on plah.Id = psah.ProcessLevelActionHistoryId
		where t.ProposalId = plah.ProposalId
	) ca
	cross apply (
		select nq.Id
		from NotificationQueue nq
		where nq.CourseId = t.CourseId
	) ca2
--where t.Id < 4000

