/*
- includes improves the queries
*/
use BikeStores;
-- ==================
-- example
-- ==================
-- create a non clustered unique index
CREATE UNIQUE INDEX ix_cust_email 
ON sales.customers(email);

--DROP INDEX ix_cust_email ON sales.customers

SELECT    
    customer_id, 
    email
FROM    
    sales.customers
WHERE 
    email = 'aide.franco@msn.com';

-- it does a "Index Seek (NonClustered)" search with the unidex index
-- without the unique index, it does a "Index Scan (Clustered)" 

-- but what if I do this:

SELECT
	first_name,
	last_name,
	email
FROM
	sales.customers
WHERE 
	email = 'aide.franco@msn.com'

-- the "query optimizer" does a "Index Seek (Non Clustered)" using the "ix_cust_email" index to find the "email" and "customer_Id"
-- second, the "query optimizer" does a "Key Lookup (Clustered)" on the clustered index of the sales.customers table to find the first name and the last name of the customer by customer id
-- third, the "query optimizer"  for each row found in the non-clustered index, it matches with rows found in the clustered index using nested loops

-- so to help reduce this key lookup cost, SQL server allows to extend the functionality of non-clustered index by "including non-key columns" I wonder why they are called non-key columns???
-- By including non-key columns in non-clustered indexes, you can create nonclustered indexes that cover more queries.
-- A index that contains all the columns referenced by a query, the index is typically referred to as "covering the query"

-- lets drop the index
DROP INDEX ix_cust_email ON sales.customers;

-- new index
CREATE UNIQUE INDEX ix_cust_email_inc
ON sales.customers(email)
INCLUDE (first_name,last_name);
	
	-- DROP INDEX ix_cust_email_inc ON sales.customers;

-- with this index, the query optimizer will solely use the non-clustered index to return the requrested data of ther query

SELECT
	first_name,
	last_name,
	email
FROM
	sales.customers
WHERE 
	email = 'aide.franco@msn.com'

-- an index with included columsn can boost performance because all the columns of the query are included in the index. This means the query optimizer can loclate all the column values withing the index and WITHOUT accessing the table or clustered index resulting in fewer I/O operations


--DBCC FREEPROCCACHE;
--DBCC FREEPROCCACHE WITH NO_INFOMSGS;
--DBCC FREESYSTEMCACHE ('SQL Plans');
--DBCC FREESYSTEMCACHE ('SQL Plans', 'LimitedIOPool');
--DBCC FREEPROCCACHE ('LimitedIOPool');