/*
Attributes of a clustered index
- A clustered index defines the order which the data is physically store in the data.
- you can applied to more than one column (I think up to 2 columns, not sure though)
- adding the primary key to a column, sql server automatically adds a clustred index
- you can create custom clustered index by "CREATE CLUSTERED INDEX [name] ON <table_name>(column [ASC | DESC], ,...n)
- if you applied the clustered index to more than one columns, the index is called an "complisite index"

Attributes of a non clustered index
- are stored in a separate place from the actual table


Resource
https://www.sqlshack.com/what-is-the-difference-between-clustered-and-non-clustered-indexes-in-sql-server/

*/