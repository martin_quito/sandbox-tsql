USE master;

-- ===========================
-- dm_exec_query_stats
-- ===========================
/*
"Returns aggregate performance statistics for cached query plans in SQL Server. The view contains one row per query statement within the cached plan, and the lifetime of the rows are tied to the plan itself. When a plan is removed from the cache, the corresponding rows are eliminated from this view."


COLUMNS
	sql_handler
		- is a binary identifer used for other purposes
	plan_handle
		- is an identifier for the query pan
OTHER
	- what is  "cahced query plans"
		- https://logicalread.com/sql-server-minimize-single-use-plans-tl01/#.WR8gFWjyvRY Another term is "SQL Server plan cache" stores the plans and when statemnets get executed, it looks in the "plan cache" pool if a plan exists, if it does, it doesn't create another plan. 
		- https://www.sqlshack.com/searching-the-sql-server-query-plan-cache/ when a queyr is executed, an executed plan is created and cached, what is cached is riched of metadata and can be used to give insights of the server's performance

*/

SELECT *
FROM sys.dm_exec_query_stats 

-- create two batches
DBCC FREEPROCCACHE
IF OBJECT_ID('dbo.Cats', 'U') IS NOT NULL
	DROP TABLE dbo.Cats
DBCC FREEPROCCACHE
CREATE TABLE dbo.Cats (Id INT)
--GO

DECLARE @i INT = 1
WHILE @i <= 100 BEGIN
	INSERT INTO dbo.Cats
	VALUES (@i)
	SET @i = @i + 1
END
GO

-- create three sqlstatemnts in one batch



-- ===========================
-- dm_exec_sql_text
-- ===========================
/*
"Returns the text of the SQL batch that is identified by the specified sql_handle. This table-valued function replaces the system function fn_get_sql."

- this is part of the "Dynamic Management objects"
- "sql_handle" is of a type "varbinary(64)" and can be obtained from 
	- sys.dm_exec_query_stats
	- sys.dm_exec_requests
	- sys.dm_exec_cursors
	- sys.dm_exec_xml_handles
	- sys.dm_exec_query_memory_grants
	- sys.dm_exec_connections
*/

-- retrieve 
SELECT deqs.last_execution_time AS [Time]
, dest.*
FROM sys.dm_exec_query_stats AS deqs
	CROSS APPLY sys.dm_exec_sql_text(deqs.sql_handle) AS dest
ORDER BY deqs.last_execution_time DESC


-- ===========================
-- dm_exec_request
-- ===========================
/*
Returns information about each request that is executing within SQL Server.
*/
SELECT *
FROM sys.dm_exec_requests



-- ===========================
-- DBCC FREEPROCCACHE
-- ===========================
/*
"Removes all elements from the plan cache, removes a specific plan from the plan cache by specifying a plan handle or SQL handle, or removes all cache entries associated with a specified resource pool."
*/

USE master
GO
CREATE TABLE dbo.Martin (Id INT)
GO
SELECT *
FROM dbo.Martin

SELECT *
FROM sys.dm_exec_query_stats 

DBCC FREEPROCCACHE
	-- 0x0200000094815034813C774CB51AD834D46FD8937A9E7128


-- ===========================
-- batches
-- ===========================
/*
Batches
- "A batch is a group of one or more Transact-SQL statements sent at the same time from an application to SQL Server for execution. SQL Server compiles the statements of a batch into a single executable unit, called an execution plan. The statements in the execution plan are then executed one at a time."
- compile error, syntax error, run-time error
- its a group of one or more T-SQL stms sent from an app to the sql server where it gets compile into one unit (execution plan) which then is exectued. 
- https://technet.microsoft.com/en-us/library/ms175502(v=sql.105).aspx
- rules
	- almost anything with the CREATE needs to have its own batch and the whole batch statements will be part of the definiton of the first create statement
	- in the same batch, the table cannot be changed and then referenced to those changed columns
	- EXECUTE statement if is the first statement in the batch, then the keyword is not required. 
*/

-- recompilation
CREATE TABLE dbo.t3(a int) ;
INSERT INTO dbo.t3 VALUES (1) ;
INSERT INTO dbo.t3 VALUES (1,1) ;
INSERT INTO dbo.t3 VALUES (3) ;
GO

SELECT * FROM dbo.t3 ;
DROP TABLE  dbo.t3
/*
A
- why does the above statements work since when it gets compiled, the insert statements will not work because the create table has not been executed? 
	- there are 2 batches. The first batch is compiled but the inserts dont get compiled because the table does not exist yet. Then the batch gets executed, the table is created, the first insert then gets compiled and then executed. The second insert statement fails which then the compilation fails and terminates the batch so the third statement does not get executed. Then, the second batch gets run which returns a one.
	

*/

-- ===========================
-- GO
-- ===========================
/*
- its not part of the T-SQL statement, but is recognized by sqlcmd and osql and SQL Server Managemente studio.

GO signals the end of a batch of Transact-sql statements to the sql server utlities
*/


-- ===========================
-- table valued user-defined functions
-- ===========================
/*
- https://technet.microsoft.com/en-us/library/ms191165(v=sql.105).aspx
- returns a TABLE data type can be an alternative to views
- views are limited to one single select statement whereas the "table-valued user-defined function' can be used where tables and views are allowed
- "A table-valued user-defined function can also replace stored procedures that return a single result set. The table returned by a user-defined function can be referenced in the FROM caluse of a Transacst-SQL statement, but stored procedures tha treturn result sets cannot"

THINGS ABOUT THE RETURN
	- defines the local variable to be returned by the function. The scope is within the function's definition
	- the only thing returned by a function is a table and nothing else
*/
USE master;
GO

IF OBJECT_ID('dbo.MyFunction', 'TF') IS NOT NULL
	-- "returns the database object identification number of a schema-coped object"
	-- TF is a tabled value (user-defined) function
	-- U is a user defined table
	DROP FUNCTION dbo.MyFunction;
GO
CREATE FUNCTION dbo.MyFunction(@arg1 INT)
-- defined the return table
RETURNS @returnTable TABLE 
(
	  FirstName NVARCHAR(50)
	, LastName NVARCHAR(50)
)
-- get the data
BEGIN
	INSERT INTO @returnTable
	VALUES
	('Martin', 'Quito')
	RETURN
END
GO

-- call the table value (user defined) function
SELECT *
FROM dbo.MyFunction(1)