-- ===========================
-- JOINS with more than one condition 
-- ==========================
DECLARE @listofNums TABLE (id INT)
INSERT INTO @listOfNums VALUES
(1), (2), (3)

DECLARE @listofNums2 TABLE (id INT)
INSERT INTO @listOfNums2 VALUES
(1), (2), (3)

--SELECT * FROM @listofNums
--SELECT * FROM @listofNums2

-- simple inner join 
--SELECT l1.Id AS L1, l2.Id AS L2
--FROM @listofNums l1
--	INNER JOIN @listofNums2 l2 ON l1.id = l2.id
	-- L1	L2
	-- 1	1
	-- 2	2
	-- 3	3

-- on List2, just do 1
SELECT l1.Id AS L1
, l2.Id AS L2
FROM @listofNums l1
	INNER JOIN @listOfNums2 l2 ON l2.id = 1
		-- L1	l2
		--	1	1
		--	2	1
		--	3	1

	-- the same as before but with a diff number that doesn't exist
	--SELECT l1.Id AS L1
	--, l2.Id AS l2
	--FROM @listofNums l1
	--	INNER JOIN @listofNums2 l2 ON l2.Id = 10
		-- nothing!


	-- on list 2, where = 1
	--SELECT l1.Id AS L1
	--, l2.Id AS l2
	--FROM @listofNums l1
	--	INNER JOIN @listofNums2 l2 ON l1.Id = l2.Id
	--WHERE l2.Id = 1
		-- L1	L2
		-- 1    1

	-- join to list 2 on 1 and where = 1
	SELECT l1.Id AS L1
	, l2.Id AS l2
	FROM @listofNums l1
		INNER JOIN @listofNums2 l2 ON l2.Id = 1
	WHERE l2.Id = 1
		-- L1	l2
		--	1	1
		--	2	1
		--	3	1
	
	-- 1=1
	SELECT l1.Id AS L1
	, l2.Id AS l2
	FROM @listofNums l1
		INNER JOIN @listOfNums2 l2 ON 1=1

		-- similar to cross joins 
		--SELECT *
		--FROM @listofNums
		--	CROSS JOIN @listofNums2

-- +1
SELECT l1.Id AS L1
, l2.Id AS l2
FROM @listofNums l1
	INNER JOIN @listOfNums2 l2 ON l1.id = l2.id + 1
	-- L1   L2
	-- 2	1 => 1+1 = 2
	-- 3	2 => 2+1 = 3

-- ==============
-- left and then an inner
-- ==============
DECLARE @List1 TABLE (id INT) INSERT INTO @List1 VALUES (1), (2), (3)
DECLARE @List2 TABLE (id INT) INSERT INTO @List2 VALUES (1), (3)
DECLARE @List3 TABLE (id INT) INSERT INTO @List3 VALUES (2), (3)

SELECT *
FROM @List1 l1
	LEFT JOIN @List2 l2 ON l1.Id = l2.id
	INNER JOIN @List2 l3 ON l2.Id = l3.id
		-- . the inner joins filters that if there is a null somewhere in the previous joins, then don't display it.