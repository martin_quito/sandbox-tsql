-- ==============================
-- string concatenation
-- ==============================
DECLARE @CourseOutcomes TABLE 
	( CourseId INT
	, OutcomeText NVARCHAR(MAX)
	, OrderNum INT)

INSERT INTO @CourseOutcomes VALUES
  (1, 'Outcome1', 1)
, (1, 'Outcome2', 2)
, (1, 'Outcome3', 3)
, (2, 'MyOutcome1', 1)
, (2, 'MyOutcome2', 2)
, (3, 'AnotherOutcome', 1)


-- we are going to use the order 1 to start with the str concatenation
;WITH joinedOutcomes (CourseId, OutcomeText, OrderNum) AS
(
	-- first select 
	SELECT CourseId, jo.OutcomeText, jo.OrderNum
	FROM @CourseOutcomes jo
	WHERE jo.OrderNum = 1

	UNION ALL

	SELECT jo.CourseId, jo.OutcomeText + ', ' + co.OutcomeText, co.OrderNum	-- the next num is the one of not of the recursive because that is the value that is being matched!
	FROM joinedOutcomes jo
		INNER JOIN @CourseOutcomes co ON jo.CourseId = co.CourseId
									AND jo.OrderNum + 1 = co.OrderNum
										-- . this will work
									--AND jo.OrderNum  = co.OrderNum + 1
										-- . this doesn't work. this way it adds the values (OrderNum) on the source table to be higher values which creates no relationship
), 
filter1 (CourseId, MaxLen) AS
(
	SELECT CourseId, MAX(LEN(OutcomeText))
	FROM JoinedOutcomes
	GROUP BY CourseId
),
finalResultFromFilter1 AS
(
	SELECT f.CourseId, jo.OutcomeText
	FROM filter1 f
		INNER JOIN joinedOutcomes jo ON f.CourseId = jo.CourseId
									AND f.MaxLen = LEN(jo.OutcomeText)
)	SELECT *
	FROM finalResultFromFilter1
		-- . here we use the filter by using the max len



-- the same way as before, but different way in filtering
;WITH joinedOutcomes (CourseId, OutcomeText, OrderNum) AS
(
	-- first select 
	SELECT CourseId, jo.OutcomeText, jo.OrderNum
	FROM @CourseOutcomes jo
	WHERE jo.OrderNum = 1

	UNION ALL

	SELECT jo.CourseId, jo.OutcomeText + ', ' + co.OutcomeText, co.OrderNum
	FROM joinedOutcomes jo
		INNER JOIN @CourseOutcomes co ON jo.CourseId = co.CourseId
									AND jo.OrderNum + 1 = co.OrderNum
	
), 
filter2 (CourseId, MaxOrderNum) AS
(
	SELECT CourseId, MAX(OrderNum)
	FROM JoinedOutcomes
	GROUP BY CourseId
),
finalResultFromFilter2 AS
(
	SELECT f.CourseId, jo.OutcomeText
	FROM filter2 f
		INNER JOIN joinedOutcomes jo ON f.CourseId = jo.CourseId
									AND f.MaxOrderNum = jo.OrderNum
)	SELECT *
	FROM finalResultFromFilter2
		-- . here we use filter by the max of the order num

-- the same as before, but with no filter
;WITH joinedOutcomes (CourseId, OutcomeText, OrderNum) AS
(
	-- first select 
	SELECT CourseId, jo.OutcomeText, jo.OrderNum
	FROM @CourseOutcomes jo
	WHERE jo.OrderNum = 1

	UNION ALL
	SELECT jo.CourseId, jo.OutcomeText + ', ' + co.OutcomeText, co.OrderNum
	FROM joinedOutcomes jo
		INNER JOIN @CourseOutcomes co ON jo.CourseId = co.CourseId
									AND jo.OrderNum + 1 = co.OrderNum
)	SELECT *
	FROM joinedOutcomes

