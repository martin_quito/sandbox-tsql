DECLARE @aTable TABLE (Id INT, FirstName NVARCHAR(MAX))
INSERT INTO @aTable VALUES
  (1, 'Martin')
, (2, 'Nefi')
, (3, 'Miriam')
, (4, 'Catherine')
, (5, 'Moises')

DECLARE @aTable2 TABLE (Id INT, LastName NVARCHAR(MAX))
INSERT INTO @aTable2 VALUES
  (1, 'Quito')
, (2, 'Lloclle')
, (3, 'Ward')

DECLARE @aTable3 TABLE (Id INT, City NVARCHAR(MAX))
INSERT INTO @aTable3 VALUES
  (1, 'Peru')
, (3, 'USA')
, (4, NULL)
, (5, 'Bolivia')

SELECT *
FROM @aTable t
	LEFT JOIN @aTable2 t2
		INNER JOIN @aTable3 t3 
			ON t2.Id = t3.Id
			ON t.Id = t2.Id
SELECT *
FROM @aTable t
	LEFT JOIN @aTable2 t2 ON t.Id = t2.Id
	INNER JOIN @aTable3 t3 ON t.id = t3.Id


SELECT *
FROM @aTable t
	LEFT JOIN @aTable2 t2 ON t.Id = t2.Id
	LEFT JOIN @aTable3 t3 ON t.Id = t3.Id

-- =====================
-- the special join allows that if there is a null in any join, then everything will return as null
DECLARE @Values1 TABLE (Id INT, Letter NVARCHAR(100))
INSERT INTO @Values1 VALUES
  (1, 'A')
, (2, 'B')
, (3, 'C')

DECLARE @Values2 TABLE (Id INT, Letter NVARCHAR(100))
INSERT INTO @Values2 VALUES
  (1, 'A')
, (3, 'C')

DECLARE @Values3 TABLE (Id INT, Letter NVARCHAR(100))
INSERT INTO @Values3 VALUES
  (2, 'B')
, (3, 'C')

-- left joins using only the base (the first)
SELECT *
FROM @Values1 v1
	LEFT JOIN @Values2 v2 ON v1.Id = v2.Id
	LEFT JOIN @Values3 v3 ON v1.Id = v3.Id
		-- 1 => 2
		-- 1 => 3

-- this is bad because what if v1 has data in v3 but not in v2, and since we are joining using v2, this will happen if a linear join is used ... it will returning NULLS
SELECT *
FROM @Values1 v1
	LEFT JOIN @Values2 v2 ON v1.Id = v2.Id
	LEFT JOIN @Values3 v3 ON v2.Id = v3.Id
		-- 1 => 2
		-- 2 => 3


-- if you do a inner join on the second join, then it will delete the other records that were join on the first join
SELECT *
FROM @Values1 v1
	LEFT JOIN @Values2 v2 ON v1.Id = v2.Id
	INNER JOIN @Values3 v3 ON v1.Id = v3.Id
	-- . the way its join is by
		-- 1 => 2 
		-- 1 => 3				


-- but, if we want to say that A (from Base) should return no matter what and return the other values from the other joins IF there is data in all the joins, if there is data in one join but not the other, then just return the value from BASE and the rest should be null
SELECT *
FROM @Values1 v1
	LEFT JOIN @Values2 v2
	INNER JOIN @Values3 v3
			ON v2.Id = v3.Id
			ON v1.Id = v2.Id
		-- .2 => 3
		-- ,1 => 2
		-- . it has to be in that order
