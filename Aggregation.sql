-- ===============
-- A simple group
-- ===============
-- a simple group by Id (number) and counting the number of elements in each group
DECLARE @table1 TABLE (Id INT, Age NVARCHAR(MAX))
INSERT INTO @table1 VALUES
  (1, 10)
, (1, 20)
, (2, 50)

SELECT Id, COUNT(Age)
FROM @table1
GROUP BY Id
	-- 1, 2
	-- 2, 1 
	-- . 2 means the number of rows in the group

-- find the maximum age in each group's ID
DECLARE @table2 TABLE (Id INT, Age NVARCHAR(MAX))
INSERT INTO @table2 VALUES
  (1, 10)
, (1, 20)
, (2, 50)

SELECT Id, MAX(Age)
FROM @table2
GROUP BY Id
	-- 1, 20
	-- 2, 50
	-- . finds the max of each group

	-- just using max without group
	SELECT MAX(Id), Max(age)
	FROM @table2
		-- 2, 50
		-- . no grouping occurs, it just returns the max of the record

-- groups by ID (number) and using MAX on strings
DECLARE @table3 TABLE (Id INT, Age NVARCHAR(MAX), FirstName NVARCHAR(MAX))
INSERT INTO @table3 VALUES
  (1, 10, 'Martin')
, (1, 20, 'Martin Quito')
, (1, 30, 'Zap')
, (2, 50, 'Martin Quito Lloclle')
, (2, 50, 'Martin Quito Lloclle e')
, (3, 60, 'Amberly')
, (3, 60, 'AmberlY')
, (4, 23, 'Bob')
, (4, 12, 'Bob ')
, (5, 33, 'Martin')
, (5, 33, 'Martin @')	-- same scenario as gorup 2
SELECT Id, MAX(FirstName)
FROM @table3
GROUP BY Id
	-- 1 Zap
	-- 2 Martin Quito Lloclle e
	-- Amberly
	-- Bob (no space up front)

	-- . For group 1, it retuns 'Zap' because Z is the highest in the alphabet
	-- . For group 2, it retuns the one with the extra 'e' because its a comparion of empty string and e which e is the highest
	-- . For group 3, y OR Y doesn't matter, it returns the lower case
	-- . For group 4, Bob with no space returns.
	-- . Max does not read the lenght of a string, so its a good candidate for that type of filtering
	
	-- here is a more effective method for length
	DECLARE @table4 TABLE (Id INT, FullName NVARCHAR(MAX))
	INSERT INTO @table4 VALUES
	  (1, 'Martin Quito')
	, (1, 'Martin Quito 1')
	, (1, 'Martin Quito Lloclle')
	, (1, 'Z')
	, (1, 'A')
	, (1, 'Z                               Z')
	, (1, 'Z                               A')
	, (1, 'ZZZZZZZZZz')
	, (2, 'Martin Quito Lloclle')
	, (2, 'Martin Quito Lloclle ')
	, (2, 'MARTIN')

	;WITH tmp (Id, MaxLen) AS
	(
		SELECT Id, MAX(LEN(FullName))
		FROM @table4
		GROUP BY Id
	)	SELECT *
		FROM tmp t
			INNER JOIN @table4 t4 ON t.Id = t4.Id AND t.MaxLen = LEN(t4.FullName)

-- MIN
-- Min works the same as max but the opposite, it follows the same rules

-- ========================
-- HAVING
-- ========================
DECLARE @table5 TABLE (Id INT, Name NVARCHAR(MAX), Value NVARCHAR(MAX)); INSERT INTO @table5 VALUES
  (1, 'MARTIN', NULL)
, (1, 'MARTIN QUITO', NULL)
, (1, 'MARTIN QUITO LLOCLLE AAAA', NULL)
, (1, 'MARTIN QUITO LLOCLLE Zasdasdsa', NULL)
, (2, 'NEFI', NULL)
, (2, 'NEFI QUITO', NULL)
, (3, NULL, NULL)
, (3, 'a', NULL)
, (4, 'b', 'a')

-- find me how many of this are duplicates 
SELECT Id, COUNT(*) AS 'How many in Group'
FROM @table5
GROUP BY Id
HAVING COUNT(*) > 1
	-- . count(*) > 1 means that what ever that result is, if is greater than one, then it will be part of the result

-- find me if the max length is 4
SELECT Id, MAX(LEN(Name))
FROM @table5
GROUP BY Id
HAVING MAX(LEN(Name)) = 30
	-- . the having clause was added to sql because the where keyword could not be used with aggrate functions | Having is where for the group results
	
-- find me if the max length is 4
SELECT Id, MAX(LEN(Name))
FROM @table5
GROUP BY Id
HAVING MAX(LEN(Name)) = 30
	-- .you have have group aggrate functions in the having function. When you group think about you are grouping based on what you have in the GROUP BY clause, then you can use what ever aggregate function you please in the select statement or in the having. THe having and select are not intercolerrated. 


-- ==================
-- Group more than one variable
-- ==================
DECLARE @supplies TABLE (Id INT, SupplyName NVARCHAR(MAX), SupplyType NVARCHAR(MAX), Quantity INT, aYear INT)
INSERT INTO @supplies VALUES
  (1, 'Table', 'Indoor', 10, 1990)
, (2, 'Monitor', 'Indoor', 5, 1990)
, (5, 'Lawn Mower', 'Indoor', 2, 2000)
, (5, 'Lawn Mower', 'Outdoor', 2, 2000)
, (5, 'Lawn Mower', 'Outdoor', 5, 2000)

SELECT SupplyName, COUNT(*)
FROM @supplies
GROUP BY SupplyName
	-- Monitor		1
	-- Table		1
	-- Lawn Mower	3

SELECT SupplyName, COUNT(*)
FROM @supplies
GROUP BY SupplyName, SupplyType


SELECT SupplyName, SupplyType, COUNT(*)
FROM @supplies
GROUP BY SupplyName, SupplyType
	-- Monitor		Indoor	1
	-- Table		Indoor	1
	-- Lawn Mower	Indoor	1
	-- Lawn Mower	Outdoor	2
	
	-- . What this says is to form a group of SupplyName and SupplyType which is 1 group is (Lawn mower and Outdoor), another group is (Lawn Mower and Indoor)
	-- . the previous queries the same thing except, it doesn't show the second group

SELECT SupplyName, SupplyType, Quantity
FROM @supplies
GROUP BY SupplyName, SupplyType, Quantity
	-- Monitor		Indoor	5
	-- Table		Indoor	10
	-- Lawn Mower	Indoor	2
	-- Lawn Mower	Outdoor	2
	-- Lawn Mower	Outdoor	5


-- =================
-- GROUP ON NULLS
-- =================
DECLARE @table7 TABLE (Id INT, Title NVARCHAR(MAX))
INSERT INTO @table7 VALUES
  (1, 'Horses')
, (NULL, 'Horses')
, (1, 'Horses')
, (2, 'Bees')
, (2, 'Cats')

SELECT Id, COUNT(Title)
FROM @table7
GROUP BY Id
	-- NULL	1
	-- 1	2
	-- 2	2
	-- . NULL is considered as a value by itself

-- =================
-- COUNT ON NULLS
-- =================
DECLARE @table6 TABLE (Id INT, Age INT)
INSERT INTO @table6 VALUES
  (1, 10)
, (1, NULL)
, (2, 20)
, (2, 30)
, (NULL, 100)
, (NULL, NULL)

SELECT COUNT(*)
FROM @table6
	-- 6
	-- . counts everything!

SELECT Id, COUNT(Age)
FROM @table6
GROUP BY Id
	-- NULL 1
	-- 1	1
	-- 2	2
	-- . Its counting 1 because "COUNT" doesn't count NULLS

SELECT Id, COUNT(COALESCE(Age, 2000))
FROM @table6
GROUP BY Id
	-- NULL 2
	-- 1	2
	-- 2	2
	-- . The coalesce helps to return a value to be count

SELECT Id, COUNT(Id)
FROM @table6
GROUP BY Id
	-- NULL 0
	-- 1    2
	-- 2    2

SELECT Id, COUNT(*)
FROM @table6
GROUP BY Id
	-- NULL 2
	-- 1	2
	-- 2	2
	-- . The * counts everything including NULLS. The STAR is bad because what if I want to count in a group that have values and not the empty ones (NULLS)?!.
	-- . Its better to use the column that you want to count. The count(column) doesn't count NULLS


-- ===========
-- removing duplicates (using )
-- ==================
DECLARE @aTable TABLE (id INT, MyId INT, firstName NVARCHAR(MAX))
INSERT INTO @aTable VALUES
  (1, 1, 'Martin')
, (2, 1, 'Martin')
, (3, 1, 'Martin')
, (4, 2, 'Amber')
, (5, 2, 'Amber')
, (6, 3, 'Allison')
, (7, 3, 'Allison')

SELECT MyId, COUNT(*)
FROM @aTable
GROUP BY MyId
HAVING COUNT (*) > 1
	-- 1, 3
	-- 2, 2
	-- 3, 2

SELECT Min(Id)
FROM @aTable
GROUP BY MyId, firstName
	

DELETE FROM @aTable
OUTPUT DELETED.*
WHERE Id NOT IN (
	SELECT Min(Id)
	FROM @aTable
	GROUP BY MyId, firstName
)

SELECT * FROM @aTable

-- ===============
-- SUM
-- ===============
DECLARE @Table TABLE (Population INT)
INSERT INTO @Table VALUEs
(10), (20) , (30), (40)

SELECT SUM(Population)
FROM @Table
	-- 10
	-- . just but it self, it sums everything


