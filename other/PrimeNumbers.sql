DECLARE @MaxNumber INT = 1000;
DECLARE @i INT = 1;
DECLARE @StringOutput NVARCHAR(MAX) = NULL;
DECLARE @k INT = 0;
DECLARE @RemainderCounter INT = 0;

WHILE(@i <= @MaxNumber) BEGIN
    
    SET @k = @i;

    WHILE(@k > 0)BEGIN
        IF(@i % @k = 0) BEGIN
            SET @RemainderCounter = @RemainderCounter + 1;
        END
        SET @k = @k - 1;
    END


    IF(@StringOutput IS NOT NULL AND @RemainderCounter = 2) BEGIN
        SET @StringOutput = @StringOutput + '&' + CAST(@i AS NVARCHAR(MAX));
    END

    IF(@StringOutput IS NULL AND @RemainderCounter = 2)BEGIN
        SET @StringOutput = CAST(@i AS NVARCHAR(MAX));
    END

    SET @RemainderCounter = 0;
    SET @i = @i + 1;
END


SELECT LEFT(@StringOutput, LEN(@StringOutput) - 0);
