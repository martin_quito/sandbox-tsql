-- ========================
-- A basic looping
-- ========================
DECLARE @Names TABLE (
	FirstName NVARCHAR(MAX)
)
INSERT INTO @Names VALUES ('Martin'), ('Miriam'), ('Nefi'), ('Catherine')

SELECT *
FROM @Names
WHERE FirstName = 'Martin'

/*
Some psudo code
- the names represents a list 
$Result = Foreach($i in $Names){
	if($i -eq "Martin")
		return $i
}
*/
SELECT *
FROM @Names
WHERE FirstName = 'Martin' OR FirstName = 'Catherine'
/*
Some psudo code
$Result = Foreach($i in $Names){
	if($i -eq "Martin" -or $i -eq "Catherine")
		return $i
}
*/

-- =====================
-- a nested loop
-- =====================
DECLARE @customers TABLE (CustomerId INT , LastName NVARCHAR(MAX), FirstName NVARCHAR(MAX))
INSERT INTO @customers VALUES 
( 4000,  'Jackson',    'Joe'         )
,(5000,  'Smith',      'Jane'        )
,(6000,  'Ferguson',   'Samantha'    )
,(7000,  'Reynolds',   'Allen'       )
,(8000,  'Anderson',   'Paige'	     )
,(9000,  'Johnson',    'Derek'       )

DECLARE @orders TABLE (OrderId INT, CustomerId INT)
INSERT INTO @orders VALUES
  (1, 5000)
, (2, 6000)
, (3, 7000)
, (4, 9000)

-- find all the records in the customers table where there is a tleast one record in the orders table with the same customer_id
SELECT *
FROM @customers c
WHERE EXISTS (
	SELECT 1
	FROM @orders o
	WHERE c.CustomerId = o.CustomerId -- if there is at least one true, it will return
)

/*
$r = foreach($i in $customers){
	foreach($k in $orders){
		-- if there is at least one record in the order table
		if($i.CustomerId = $k.CustomerId){
			return $i -- restart the outer loop with a new value
		}
	}
}

show $r
*/

-- ========================
-- Nested loop with more conditions
-- ========================
DECLARE @customers1 TABLE (CustomerId INT, FullName NVARCHAR(MAX))
INSERT INTO @customers1 VALUES (1, 'Nicasio Quito'), (2, 'Juana Quito'), (3, 'Miriam Quito'), (4, 'Nefi Quito'), (5, 'Martin Quito'), (6, 'Moises Quito'), (7, 'Catherine Quito')
DECLARE @orders1 TABLE (OrderId INT, CustomersId INT, Quantity INT, isShipped BIT)
INSERT INTO @orders1 VALUES 
  (100, 1, 2, 0) -- nicasio
, (200, 1, 1, 0) -- nicasio
, (300, 1, 4, 0) -- nicasio
, (400, 2, 1, 0) -- juana
, (500, 2,-1, 1) -- juana
, (600, 4, 8, 1) -- nefi
, (700, 5, 4, 0) -- martin
, (800, 5, 4, 1) -- martin


SELECT *
FROM @customers1 c
WHERE EXISTS (
	SELECT *
	FROM @orders1 o
	WHERE c.CustomerId = o.CustomersId 
	AND o.Quantity = 4 AND o.isShipped = 1
)


-- ============================
-- 
-- ============================
DECLARE @Shoes TABLE (Id INT, Brand NVARCHAR(MAX), Size INT)
INSERT INTO @Shoes VALUES 
  (1, 'Nike', 5)
, (2, 'Nike', 6)
, (3, 'Nike', 7)
, (4, 'Adidas', 8)
, (5, 'Adidas', 8)
, (6, 'Puma', 5)
, (7, 'Puma', 5)
, (8, 'Joys', 4)
, (9, 'Joysaa', 4)
, (10, 'Joys', 5)
, (11, 'Vans', 9)
-- give me all the brands that have more than one record in the shoe table and that in each brand, the sizes are different. If they are the same, don't return. 
-- two things, more than one record, and there is a difference between those two
--;WITH grouped AS
--(
--	SELECT Brand, COUNT(Brand) AS 'Count'
--	FROM @Shoes
--	GROUP BY Brand
--	HAVING COUNT(*) > 1
--)	SELECT DISTINCT s.*
--	FROM grouped g
--		INNER JOIN @Shoes s ON g.Brand = s.Brand
--		INNER JOIN @Shoes s2 ON s.Brand = s2.Brand
--						AND s.Size != s2.Size

-- another way to do it
--SELECT *
--FROM @Shoes s
--WHERE  EXISTS (
--	SELECT *
--	FROM @Shoes sh
--	WHERE s.Brand = sh.Brand
--	AND s.Size != sh.Size
--)

/*
Grabs "Nike 5"
	- it then compares it to:
		* Nike 5 (5 not equal to 5) => FALSE
		* Nike 6 (5 not equal to 6) => TRUE
		* Nike 7 (5 not equal to 7) => TRUE
	- it returns "Nike 5" because there is at least one true
Grabs "Nike 6" and "Nike 7" and same as "Nike 5"

Grabs "Adidas 8"
	- it then compares it to:
		* Adidas 8 (8 is not equal to 8) => FALSE
		* Adidas 8 (8 is not equal to 8) => FALSE
	- it does not return
...

-- . if at least one returns from the exist subquery, it will return!. It is a nested loop where there is an IF statuement with the conditions and if is true, then return and then "continue"
-- . This model is used when you want to want to find if there is more than one item in the same table and if the id is different. This is based on a two columns with no primary key. If there are duplicatives, then it will not return it. For duplicatives, there is another query for it of return the duplicative or returngin the original one. 

*/

-- if more than one brand have the same number of size.
SELECT *
FROM @shoes s
WHERE EXISTS (
	SELECT 1
	FROM @Shoes s1
	WHERE s.Size = s1.Size
	AND s.Id != s1.Id
)

SELECT DISTINCT s2.*
FROM @Shoes s
	INNER JOIN @Shoes s2 ON s.Size = s2.Size
						AND s.Id != s2.Id






-- ==================================
-- other
-- ==================================
DECLARE @table TABLE (BaseCourseId INT, StatusTitle NVARCHAR(MAX))
INSERT INTO @table (BaseCourseId, StatusTitle) VALUES
  (1, 'Draft')
, (1, 'In Review')

, (2, 'Approve')
, (2, 'Draft')

, (3, 'Active')
, (3, 'In Review')

, (4, 'Active')
, (4, 'Approve')

, (5, 'Active')
, (5, 'Active')
, (5, 'Historical')

, (6, 'Approve')
, (6, 'In Review')

SELECT t1.BaseCourseId, t1.StatusTitle
FROM @table t1
	INNER JOIN @table t2 ON t1.BaseCourseId = t2.BaseCourseId
						AND t1.StatusTitle != t2.StatusTitle
WHERE (t1.StatusTitle = 'Draft' AND t2.StatusTitle = 'In Review')
OR (t1.StatusTitle = 'Approve' AND t2.StatusTitle = 'Draft')
OR (t1.StatusTitle = 'Approve' AND t2.StatusTitle = 'In Review')
GROUP BY t1.BaseCourseId, t1.StatusTitle
HAVING COUNT(t1.StatusTitle) = 1
ORDER BY t1.BaseCourseId
