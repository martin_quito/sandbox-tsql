-- ==================
-- EXISTS
-- ==================
/*
	- "specifies a subquery to test for the existence of rows"

	- "When a subquery is introdcuted with the keyword EXISTS, the subquery functions as an existence test. The WHERE clause of the outer query tests whether the rows that are returned by the subquery exists. The subquery does not actually produce any data; it returns a value of TRUE or FALSE.

	- "The SQL EXISTS condition is used in combination with a subquery and is considered to be met, if the subquery returns at least one row. It can be used in a SELECT, INSERT, UPDATE, or DELETE statement."
		- SYNTAX "WHERE EXISTS (subquery);"
		- "Parameters or Arguments"
			- "The subquery is A SELECT statement. If the subquery returns at least one record in its result set, the EXISTS clause will evaluate to true and THE EXISTS condition will be met. If the subuqery does not return any records, the EXISTS clause will evaluate to false and the EXISTS condition will not be met."
*/


DECLARE @customers TABLE (CustomerId INT , LastName NVARCHAR(MAX), FirstName NVARCHAR(MAX))
INSERT INTO @customers VALUES 
( 4000,  'Jackson',    'Joe'         )
,(5000,  'Smith',      'Jane'        )
,(6000,  'Ferguson',   'Samantha'    )
,(7000,  'Reynolds',   'Allen'       )
,(8000,  'Anderson',   'Paige'	     )
,(9000,  'Johnson',    'Derek'       )

DECLARE @orders TABLE (OrderId INT, CustomerId INT)
INSERT INTO @orders VALUES
  (1, 5000)
, (2, 6000)
, (3, 7000)
, (4, 9000)

-- find all the records in the customers table where there is a tleast one record in the orders table with the same customer_id
SELECT *
FROM @customers c
WHERE EXISTS (
	SELECT 1
	FROM @orders o
	WHERE c.CustomerId = o.CustomerId
)

	-- another way	
	SELECT *
	FROM @customers c
		INNER JOIN @orders o ON c.CustomerId = o.CustomerId

-- find all the records in the customers table where there is not a record in the orders tabl with the same customer_id
SELECT *
FROM @customers c
WHERE NOT EXISTS (
	SELECT 1
	FROM @orders o
	WHERE c.CustomerId = o.CustomerId
)
	-- another way	
	SELECT *
	FROM @customers c
		LEFT JOIN @orders o ON c.CustomerId = o.CustomerId
	WHERE o.OrderId IS NULL


-- ==================
-- adding more conditions inside the exist clause
-- ==================
DECLARE @customers1 TABLE (CustomerId INT, FullName NVARCHAR(MAX))
INSERT INTO @customers1 VALUES (1, 'Nicasio Quito'), (2, 'Juana Quito'), (3, 'Miriam Quito'), (4, 'Nefi Quito'), (5, 'Martin Quito'), (6, 'Moises Quito'), (7, 'Catherine Quito')
DECLARE @orders1 TABLE (OrderId INT, CustomersId INT, Quantity INT, isShipped BIT)
INSERT INTO @orders1 VALUES 
  (100, 1, 2, 0) -- nicasio
, (200, 1, 1, 0) -- nicasio
, (300, 1, 4, 0) -- nicasio
, (400, 2, 1, 0) -- juana
, (500, 2,-1, 1) -- juana
, (600, 4, 8, 1) -- nefi
, (700, 5, 4, 0) -- martin
, (800, 5, 4, 1) -- martin

-- get me all the customers where there is at least one record in the order table AND at least one order record is greater than 1
SELECT *
FROM @customers1 c
WHERE EXISTS (
	SELECT 1
	FROM @orders1 o
	 WHERE c.CustomerId = o.CustomersId AND o.Quantity > 1
)
	--1	Nicasio Quito
	--4	Nefi Quito
	--5	Martin Quito

-- get me all the customers where there is at least one record in the order table AND at least one order record is 4
SELECT *
FROM @customers1 c
WHERE EXISTS (
	SELECT *
	FROM @orders1 o
	WHERE c.CustomerId = o.CustomersId AND o.Quantity = 4
)
	--1	Nicasio Quito
	--5	Martin Quito

	-- . the way I see this works is it links the records from each table. For example, nicasio in the customer table is linked with the 3 orders in the orders table. THere are 3 rows. If all rows comes false, then nicasio will not show. If at least one row is true, then it will show. 2 rows are FALSE because of quantity 4. 1 row is TRUE because of quantiy 4. So, nicasio shows. 
	-- . this al

-- get me all the customers where there is at least one record in the order table OR at least one order record is 4
SELECT *
FROM @customers1 c
WHERE EXISTS (
	SELECT 1
	FROM @orders1 o
	WHERE c.CustomerId = o.CustomersId OR o.Quantity = 4
)
	-- returns everything from the customer table
	-- . this is due because there is a quantity of value 4 so everything will return. if there is a quantity value of 4 in the orders table, then everything will return

-- get me all the customers where there is at least one record in the order table AND at least one order record is 4 AND is shipped
SELECT *
FROM @customers1 c
WHERE EXISTS (
	SELECT *
	FROM @orders1 o
	WHERE c.CustomerId = o.CustomersId AND o.Quantity = 4 AND o.isShipped = 1
)
	-- 5  Martin Quito


-- get me all the customers where at least one of the orders of each customer have been shipp
SELECT *
FROM @customers1 c
WHERE EXISTS (
	SELECT 1
	FROM @orders1 o
	WHERE c.CustomerId = o.CustomersId AND o.isShipped = 1
)
	--2	Juana Quito
	--4	Nefi Quito
	--5	Martin Quito



-- ==================
-- adding more conditions inside the NOT exist clause
-- ==================
DECLARE @customers2 TABLE (CustomerId INT, FullName NVARCHAR(MAX))
INSERT INTO @customers2 VALUES (1, 'Nicasio Quito'), (2, 'Juana Quito'), (3, 'Miriam Quito'), (4, 'Nefi Quito'), (5, 'Martin Quito'), (6, 'Moises Quito'), (7, 'Catherine Quito')
DECLARE @orders2 TABLE (OrderId INT, CustomersId INT, Quantity INT, isShipped BIT)
INSERT INTO @orders2 VALUES 
  (100, 1, 2, 0) -- nicasio
, (200, 1, 1, 0) -- nicasio
, (300, 1, 4, 0) -- nicasio
, (400, 2, 1, 0) -- juana
, (500, 2,-1, 1) -- juana
, (600, 4, 8, 1) -- nefi
, (700, 5, 4, 0) -- martin
, (800, 5, 4, 1) -- martin

-- get me all the customers that don't have any orders
--SELECT *
--FROM @customers2 c
--WHERE NOT EXISTS (
--	SELECT 1
--	FROM @orders2 o
--	WHERE c.CustomerId = o.CustomersId
--)
	--3	Miriam Quito
	--6	Moises Quito
	--7	Catherine Quito

-- get me all the customers that don't have any orders and which orders are not of quantity greater of 1
--SELECT *
--FROM @customers2 c
--WHERE NOT EXISTS (
--	SELECT 1
--	FROM @orders2 o
--	WHERE c.CustomerId = o.CustomersId AND o.Quantity > 1 -- at least one, if is true the don't show 
--)
	--2	Juana Quito
	--3	Miriam Quito
	--6	Moises Quito
	--7	Catherine Quito

	-- if those 2 condtioons are TRUE, don't return the item. For instance, nicasio, if nicasio has at least one record in the order table where its id is in the order table and the quantity is greater than 1, the don't return it. Don't return nicasio. However, for example juana, there is a record of juana in the orders table but the quantiy is less than 1, then it will return, juana will return.

-- get me all the customes that don't have any orders and the shipment is not 1
SELECT *
FROM @customers2 c
WHERE NOT EXISTS (
	SELECT 1
	FROM @orders2 o
	WHERE c.CustomerId = o.CustomersId AND o.isShipped = 1
)
	--1	Nicasio Quito
	--3	Miriam Quito
	--6	Moises Quito
	--7	Catherine Quito
