<<<<<<< Updated upstream
-- ===============
-- EXISTS
-- ===============
-- specifies a subquery to test for the existence of rows

DECLARE @tmp TABLE (id INT);
INSERT INTO @tmp VALUES (2),(3);

IF(EXISTS (SELECT * FROM @tmp))
BEGIN
	SELECT * FROM @tmp;
END;
	-- if rows exists 
	
IF (NOT EXISTS (SELECT * FROM @tmp))
BEGIN 
	SELECT * FROM @tmp
END;
	-- if no row exists

-- ======= another use ========
DECLARE @a TABLE (id INT, name NVARCHAR(MAX))
INSERT INTO @a VALUES (1, 'Martin'), (2, 'Martin'), (3, 'Martin')

SELECT * 
FROM @a t
WHERE  EXISTS (SELECT 1
				  FROM @a d
				  WHERE t.id >d.id)
		-- 2 > 1, 3 > 1 those are true which will be in the top queyr result 

-- EXISTS mean that waht ever the query gives, that is waht we are going to get, NOT exist means what ever the query gives, those records will be removed from the final query

-- ===============
-- SELECT 1
-- ===============
DECLARE @tmp TABLE (id INT);
INSERT INTO @tmp VALUES (2),(3);

SELECT 1000 FROM @tmp
SELECT TOP 1 Id FROM @tmp
SELECT TOP 2 Id FROM @tmp

	-- . generates 1 (no column name) with 2 rows of 1000
	-- . This ouputs the number(or char) with the number of #rows of the seleted @tmp

-- doesn't work --
SELECT TOP * FROM @tmp

	
-- ==============
-- SIMPLE COMMANDS
-- ==============
PRINT 'aaaahhh!!!'
SET NOCOUNT ON;
	-- ON stops the message that shows the count of the number of rows affected by a Transact-SQL statement or stored procedure from being returned as part of the result set.

-- ==============
-- WITH
-- ==============
-- (1)with vs select
SELECT 'dummy' as 'column'

;WITH aWithStatement AS
(
	SELECT 'dummy' as 'column'
)SELECT * FROM aWithStatement
	-- . "conceptually, the with clause allows us give us names to predefined SELECT statements inside the context of a LARGER SELECT statement. We can then reference the NAMED SELECT statements later. Now consider this next sequence of SELECT statements. 
	-- . you have to query the named query after it has been defined	


	
-- (2)reference a named query any number of times
;WITH a1 AS
(
	SELECT 'dummy' AS 'column'
)SELECT * FROM a1, a1
	-- this doesn't work because i have to suffix the tablles
;WITH a1 AS
(
	SELECT 'dummy' AS 'column'
)SELECT * FROM a1 x, a1 y
	-- this works, we can reference a named query any number of times
	

-- (3)create any number of named queries
;WITH tmp1 AS
(
	SELECT 'hello' AS 'column', 'MartinQuito' AS 'column_name'
	UNION
	SELECT 'goodmorning', 'MiriamQuito'
	
), tmp2 AS
(
	SELECT 'bye' AS 'column1'
), tmp3 AS
(
	SELECT 'hi' AS 'column2'
) SELECT * FROM tmp1, tmp2, tmp3
	-- . works fine
	-- . at least one with needs to be called to not receive exceptions
	
-- (4)named queries can reference other named queries that came before them and even correlate to previous named queries
;WITH tmp1 AS
(
	SELECT 'hello' AS 'column'
), tmp2 AS
(
	SELECT 
	  (SELECT * FROM tmp1)	as 'x'
	, 'bye'					as 'y'
)SELECT * FROM tmp2
	-- . "named queries are only good for the SELECT statement that names them, their scope is local to the SELECT in which they are defined, hence no sharing across statements."
	
-- (5) with with insert
DECLARE @tmp TABLE (id INT, StudentName NVARCHAR(MAX));
;WITH select1 AS
(
	SELECT 1 AS 'Student Id'
	, 'Martin Quito' AS 'Student'
)
INSERT INTO @tmp
SELECT * FROM select1
SELECT * FROM @tmp -- query tmp table
	-- after your SELECT has been built, you can then inserted into a table
	
	
-- =============
-- ltrim AND rtrim
-- =============
-- ltrim. returns a character expression after it removes leading blanks
DECLARE @name NVARCHAR(MAX) = ' Martin '
SELECT LTRIM(@name), LEN(@name) AS 'before', LEN(LTRIM(@name)) as 'after';
	-- 'Martin '
	-- removes the spaces before
	
-- rtrim. returns a character string after truncating all trailing blanks
DECLARE @name NVARCHAR(MAX) = ' Martin '
SELECT RTRIM(@name), LEN(@name) AS 'before', LEN(RTRIM(@name)) as 'after';
	-- ' Martin'
	-- removes the spaces after

-- . to remove spaces, I'll have to first aplply the rtrim and then the ltrim





-- =============
-- JOINS
-- =============
-- LEFT JOIN. "returns all rows from the left table, even if there is no matches int he right table. This means that if the ON cluases matches 0 (zero) records in right table, the join will still return a row in the result, but with NULL in each column from right table. This means that a left join returns all the values from the left table, plus matched values from the right table OR NULL in case of no matching join predicate"
;WITH departments AS
(
	SELECT 1 as 'department_id', 'english' as 'department_name'
	UNION
	SELECT 2 as 'department_id', 'math' as 'department_name'
	UNION 
	SELECT 3 as 'department_id', 'biology' as 'department_name'
	UNION
	SELECT 4 as 'department_id', 'IT' as 'department_name'
), teachers AS
(
	SELECT 1 as 'teacher_id', 'Mr.Bob' as 'teacher_name', 1 as 'department_id'
	UNION
	SELECT 2 as 'teacher_id', 'Mr.Nancy' as 'teacher_name', NULL as 'department_id'
	UNION
	SELECT 3 as 'teacher_id', 'Mr.Peter' as 'teacher_name', 2 as 'department_id'
	UNION
	SELECT 4 as 'teacher_id', 'Mr.SpongeBob' as 'teacher_name', NULL as 'department_id'
)	SELECT * FROM teachers t
				FULL OUTER JOIN departments dp ON t.department_id = dp.department_id
	
	--. when doing a select UNION, the first SELECT column name takes precedence.
	--. INNER JOIN returns rows when there is a match in both tables.
	--. LEFT JOIN returns all rows from the left table, even if there are no matches in the right table.
	--. RIGHT JOIN returns all rows from the right table, even if there is no matches in the left table

-- =============
-- MERGE
-- =============

-- target. this represents the books that are or were available at a functional book retailer. if quantity is 0 then is sold out
DECLARE @BookInventory TABLE (TitleID INT NOT NULL PRIMARY KEY
							, Title NVARCHAR(100) NOT NULL
							, Quantity INT NOT NULL);
							
-- source. this represents books for which an order has been placed and delivered ( we adding more books to the inventory). if quantity value for a book listed in this table is 0, then the book had been requeted but not included with the delivery. The
DECLARE @BookOrder TABLE(	TitleID INT NOT NULL PRIMARY KEY
						,   Title NVARCHAR(100) NOT NULL
						,   Quantity INT NOT NULL);
						
INSERT INTO @BookInventory VALUES 
	(1, 'The Catcher in the Rye', 6),
	(2, 'Pride and Prejudice', 3),
	(3, 'The Great Gatsby', 0),
	(5, 'Jane Eyre', 0),
	(6, 'Catch 22', 0);

INSERT INTO @BookOrder VALUES
  (1, 'The Catcher in the Rye', 3),
  (3, 'The Great Gatsby', 0),
  (4, 'Gone with the Wind', 4),
  (5, 'Jane Eyre', 5),
  (7, 'Age of Innocence', 8);
  
-- WHEN MATCHED clause
-- . "The first MERGE clause we�ll look at is WHEN MATCHED. You should use this clause when you want to update or delete rows in the target table that match rows in the source table. Rows are considered matching when the joined column values are the same.For example, if the BookID value in the BookInventory table matches the BookID value in the BookOrder table, the rows are considered to match, regardless of the other values in the matching rows. When rows do match, you can use the WHEN MATCHED clause to modify data in the target table. Lets look at an example to demonstrate how this works."

-- WHEN NOT MATCHED cluase
-- . "The next clause in the MERGE statement we�ll review is WHEN NOT MATCHED [BY TARGET]. (The BY TARGET keywords are optional.) You should use this clause to insert new rows into the target table. The rows you insert into the table are those rows in the source table for which there are no matching rows in the target. For example, the BookOrder table contains a row for Gone with the Wind. However, the BookInventory table does not contain this book. The following example demonstrates how to include a WHEN NOT MATCHED clause in your MERGE statement that adds Gone with the Wind to your target table:"

-- what we are doing here is we are updating the inventory when the source and target matches using a column key
MERGE INTO @BookInventory bi -- target
	USING @BookOrder bo			 -- table source
	ON bi.TitleID = bo.TitleID -- merge_search_condiition
	
	WHEN MATCHED THEN	-- use this to update or delete
		UPDATE
		SET bi.Quantity = bi.Quantity + bo.Quantity
		
		--1	The Catcher in the Rye	9
		--2	Pride and Prejudice		3
		--3	The Great Gatsby		0
		--5	Jane Eyre				5
		--6	Catch 22				0
		-- . you can either update or delete
	
	WHEN NOT MATCHED BY TARGET THEN	-- rows you inserte into the table are those rows in the SOURCE table for which there were no matching rows in the targetr. When you have rows that are in the source but not in target, insert the source. When NOT matched by target means "when rows not found in target" then insert rows from the sourec.
		INSERT (TitleID, Title, Quantity)
		VALUES (bo.TitleID, bo.Title, bo.Quantity)
		
		--1	The Catcher in the Rye	9
		--2	Pride and Prejudice		3
		--3	The Great Gatsby		0
		--4	Gone with the Wind		4	-- INSERTED
		--5	Jane Eyre				5
		--6	Catch 22				0
		--7	Age of Innocence		8	-- INSERTED
	
	WHEN NOT MATCHED BY SOURCE -- delete rows from the target table that does not match a row in the source table. When you have data in the source table that has not relationship in the target...then delete the target data.  
		AND bi.Quantity = 0 THEN
		DELETE
		--1	The Catcher in the Rye	9
		--2	Pride and Prejudice		3
		--3	The Great Gatsby		0
		--4	Gone with the Wind		4
		--5	Jane Eyre				5
		--7	Age of Innocence		8
		-- (Catch 22) was deleted

	OUTPUT
		$action, DELETED.*, INSERTED.*;
	
SELECT * FROM @BookInventory




-- union: ditinct
-- union all: allows duplicatives


-- =============
-- CHARINDEX
-- =============
-- Searches an expression for another expression and returns its starting position if found.
-- CHARINDEX ( expressionToFind ,expressionToSearch [ , start_location ] ) 

DECLARE @document varchar(64);
SELECT @document = 'martin quito';
SELECT CHARINDEX('quito', @document);
	-- 8
	-- starts with index 1

DECLARE @phrase NVARCHAR(MAX) = 'martin quito lloclle'
SELECT CHARINDEX('=', @phrase)

--- what if I do it with multiple chars
DECLARE @name NVARCHAR(MAX) = 'Martin Quito Lloclle'
SELECT CHARINDEX('lloc', @name)
SELECT CHARINDEX('llocz', @name)
	-- . as stated before, it returns the starting positions
	-- the llocz didn't work because didn't find with that complete chunk of string

-- =============
-- PATINDEX
-- =============
/*
The CHARINDEX and PATINDEX functions return the starting position of a pattern you specify. PATINDEX can use wildcard characters, but CHARINDEX cannot.
*/
DECLARE @document2 varchar(64);
SELECT @document2 = 'martin quito';

-- SELECT PATINDEX(PatternChar, ExpressionChar)
SELECT PATINDEX('%q%', @document2)

-- =============
-- LEFT AND RIGHT
-- =============
-- returns the left part of a character string with the specified number of characters
-- index starts at 1

-- LEFT ( character_expression , integer_expression )
DECLARE @Name NVARCHAR(60) = 'Martin Quito';
SELECT LEFT(@name, 3);
	-- subtrings the first two characters (left side)

-- RIGHT ( character_expression , integer_expression )
DECLARE @Name1 NVARCHAR(60) = 'Martin Quito';
SELECT RIGHT(@name1, 3);
	-- subtrings the last three characters (right side)	


-- ===========
-- CONVERT
-- ===========
-- converts an expression of one data type to another in SQL Server 2016.
-- Syntax for CONVERT:
-- CONVERT ( data_type [ ( length ) ] , expression [ , style ] )
DECLARE @int INT = '2';
SELECT CONVERT(INT, @int);

/* 
CAST vs CONVERT
- cast is more standard, but limited. Convert is like an enhanced type of cast. It is T-SQL standard
*/


-- =========
-- STUFF
-- =========
-- The STUFF function inserts a string into another string. It deletes a specified length of characters in the first string at the start position and then inserts the second string into the first string at the start position
-- STUFF ( character_expression , start , length , replaceWith_expression )
	-- character_expression is the base string
	-- replaceWith_expression is the substring you want to insert
SELECT STUFF('abcdef', 2, 3, 'martin');
	-- abcdef - base string
	-- 2 - where it starts
	-- 3 - length starting from 2 (bcd)
	-- martin - what to be inserted

-- ==========
-- STRIPPING OUT values
-- ==========
SELECT 1
WHERE 'ENG/ENGR' = 'ENGR' 
OR ( 'ENG/ENGR' LIKE 'ENGR/%')
OR ( 'ENG/ENGR' LIKE '%/ENGR')
OR ( 'ENG/ENGR' LIKE '%/ENGR/%')

;WITH tmp AS
(
	SELECT 'Martin, Quito' AS 'Name'
)SELECT *
 FROM tmp t
	INNER JOIN tmp o ON t.Name = 


-- ========
-- search all tables
-- ========
EXEC sys.sp_MSforeachtable 'SELECT * FROM ?'


-- ========
-- nullif
-- ========
-- return NULL if 2 params are equal
-- NULL IF!!!!!!! ... 
SELECT NULLIF(2,2)
	-- NULL
	-- .if both are the same, return NULL

SELECT NULLIF('3', '2')
	-- 3
	-- .if different, return the first expression
	
SELECT 2 / NULLIF(NULL, 0)
	-- ERROR  "The type of the first argument to NULLIF cannot be the NULL constant because the type of the first argument has to be known."

SELECT 2 / NULLIF(0 , 1)
	-- ERROR "Divide by zero error encountered."

SELECT 2 / NULLIF(1, 0)
	-- 2
	
	
SELECT 2/NULL
	-- NULL
SELECT 2/0
	-- ERROR


-- =====================
-- Concatenate strings
-- =====================
CREATE TABLE #YourTable ([ID] INT, [Name] NVARCHAR(MAX), [Value] INT)

INSERT INTO #YourTable ([ID],[Name],[Value]) VALUES (1,'Aaaaa',4)
INSERT INTO #YourTable ([ID],[Name],[Value]) VALUES (1,'Bdasdasdasd',8)
INSERT INTO #YourTable ([ID],[Name],[Value]) VALUES (1,'Casdasdas',8)
INSERT INTO #YourTable ([ID],[Name],[Value]) VALUES (2,'Casdasdas',9)

SELECT [ID]
,  STUFF(
			   (SELECT  N', ' + [Name] + N':' + CAST([Value] AS VARCHAR(MAX)) 
				FROM #YourTable 
				WHERE (ID = Results.ID) 
				FOR XML PATH(''),TYPE).value(N'(./text())[1]',N'NVARCHAR(MAX)') -- xml path allows to return more than one record for the subquery which then retti
  ,1,2,'') 
FROM #YourTable Results
GROUP BY ID

DROP TABLE #YourTable
-- ===================
-- FOR XML
-- ===================
-- A SELECT query returns results as a rowset. You can optionally retrieve formal results of a SQL query as XML by specifying the FOR XML clause in the quer
SELECT 'abbb'
FOR XML PATH;
	-- <row>abbb</row>
	
SELECT 'abbb'
FOR XML PATH('')
	-- abbb
	
SELECT (SELECT 'abbb' FOR XML PATH(''), TYPE)
	-- abbb
	-- . returns with no column name 
	
SELECT (SELECT 'abbb' FOR XML PATH(''), TYPE).value('(./text())[1]', 'NVARCHAR(MAX)')
	-- . turns the abbb into xml and then retrieves the value 
	-- .value(XQUERY, SQLTYPE)
	-- .value extracts a value from an XML instance
	

-- ========
-- finding . 
-- ========
SELECT (CASE
	WHEN '320109' NOT LIKE '%.%' THEN STUFF('320109', 3, 0, '.')
END)

SELECT (CASE
	WHEN '320109' NOT LIKE '%.%' THEN STUFF('320109', 3, 0, '.')
END)


-- ====================
-- where 
-- ====================
DECLARE @table TABLE (id INT, firstName NVARCHAR(MAX), lastName NVARCHAR(MAX))
INSERT INTO @table
VALUES
(1, 'Martin', 'Quito')
, (2, 'Martin', 'Quito')
, (3, 'Miriam', 'Quito')
, (4, 'Nefi', 'Quito')

SELECT * 
FROM @table
WHERE Id <> 4
AND firstName <> 'Nefi'
	-- my prediction is Id: 1, 2, 3
	-- 1, 2, 3


-- ====================
-- remove last perioid
-- ====================
SELECT STUFF('Participation.',  LEN('Participation.'), 1,'')




-- ==================
-- Change title first letter to be capitalized and other lower case
-- ==================
DECLARE @Index          INT
DECLARE @Char           CHAR(1)
DECLARE @PrevChar       CHAR(1)
DECLARE @OutputString   VARCHAR(255)
DECLARE @InputString NVARCHAR(MAX) = 'INTRODUCTION''S TO !CULTURAL ANTHROPOLOGY'

SET @OutputString = LOWER(@InputString)
SET @Index = 1

WHILE @Index <= LEN(@InputString)
BEGIN
    SET @Char     = SUBSTRING(@InputString, @Index, 1)
    SET @PrevChar = CASE WHEN @Index = 1 THEN ' '
                         ELSE SUBSTRING(@InputString, @Index - 1, 1)
                    END
    -- if prev has the following then make the next char UPPER
    IF @PrevChar IN (' ', ';', ':', '!', '?', ',', '.', '_', '-', '/', '&', '''', '(')
    BEGIN
		-- excep that if the prev has ' and the following has s, then its lower case
        IF (@PrevChar != '''' OR UPPER(@Char) != 'S')
			BEGIN SET @OutputString = STUFF(@OutputString, @Index, 1, UPPER(@Char)); END
    END
    SET @Index = @Index + 1
END


-- =====================
-- SUBSTRING
-- =====================
-- removes a string from a string by recibing the index and length
DECLARE @a NVARCHAR(MAX) = 'Martin'
SELECT SUBSTRING(@a, 1, 1)
	-- 'M'
	-- .parameters(Text, start, length)

DECLARE @b NVARCHAR(MAX) = 'Martin'
SELECT SUBSTRING(@b, 1)
	-- Error: requires 3 arguments

DECLARE @c NVARCHAR(MAX) = 'Martin'
SELECT SUBSTRING(@c, NULL, NULL)
	-- NULL
	
-- =================
-- ROW_NUMBER
-- =================
-- Returns a sequential number of row within a partition of a result set, starting at 1 for the first row in each partition
-- parameters
	-- order_by clause | The ORDER BY by clause determines the sequence in which the rows are assigned their unique ROW_NUMBER within a specified parition. It is required.
	-- PARTITION BY value_expression | Divides the result set produced by the FROM clause into partitions to which the ROW_NUMBER function is applied. "value_expression" specifies the column by which the result is parition. If PARTITION BY is not specified, the function treats all rows of the query result set as a single group.


-- simple
;WITH tmp (id, name) AS
(
	SELECT 1, 'martin'
	UNION 
	SELECT 2, 'nefi'
	UNION
	SELECT 3, 'miriam'
)SELECT id, name, ROW_NUMBER() OVER (ORDER BY id)
 FROM tmp
 
 -- random ids
 ;WITH tmp (id, name) AS
(
	SELECT 12, 'martin'
	UNION 
	SELECT 100123, 'nefi'
	UNION
	SELECT 10, 'miriam'
)SELECT id, name, ROW_NUMBER() OVER (ORDER BY id)
 FROM tmp
	--. the ids are treated as text

 -- ==================================
 DECLARE @ad TABLE (Id INT, Name NVARCHAR(MAX))
 INSERT INTO @ad VALUES
  (12, 'martin')
 , (100123, 'nefi')
 , (1, 'miriam')
 SELECT Id, Name, ROW_NUMBER() OVER (ORDER BY Id)
 FROM @ad 
	-- .the order of the id is based on the Id. 

-- ==================================
-- updating the sort-order
DECLARE @df TABLE (Id INT, Name NVARCHAR(MAX), SortOrder INT)
INSERT INTO @df VALUES
 	 (1, 'Martin', NULL)
	,(2, 'Nefi', NULL)
	,(3, 'Miriam', NULL)

--UPDATE @df
--SET SortOrder = (ROW_NUMBER() OVER (ORDER BY Id))
	-- window functions can only appear in the SELECT or ORDER BY clauses
	
UPDATE  t
SET SortOrder = a.rowNum
FROM @df t
	INNER JOIN (SELECT Id, (ROW_NUMBER() OVER (ORDER BY Id)) as 'rowNum' FROM @df) a ON t.Id = a.Id
	
SELECT * FROM @df


-- using the partition
DECLARE @aTable TABLE (Id INT, Name NVARCHAR(MAX), Gender NVARCHAR(MAX), SortOrder INT)
INSERT INTO @aTable VALUES
 	 (1, 'Martin', 'Male',NULL)
	,(2, 'Nefi', 'Male',NULL)
	,(3, 'Miriam', 'Female', NULL)
SELECT Id, Name, Gender, (ROW_NUMBER() OVER (PARTITION BY Gender ORDER BY Id))
FROM @aTable 
ORDER BY Id



-- ==========================
-- Recursive
-- ==========================
-- the most simplest recursive CTE
;WITH countMe (Number) AS
(
	SELECT 1				-- anchor
	UNION ALL
	
	SELECT Number +1
	FROM countMe
	WHERE Number + 1 <= 3	-- recursive member
)SELECT * 
 FROM countMe
 
 -- factorial
 ;WITH fac(n, n1) AS
 (
	SELECT 1, 1
	UNION ALL
	
	SELECT n + 1, n + 1
	FROM fac
	WHERE n + 1 < 99
	
 )SELECT * 
  FROM fac

-- ---------
-- first try to do grouped concatenation 
-- ---------
DECLARE @tmeow TABLE (Id INT, aText NVARCHAR(MAX))
INSERT INTO @tmeow VALUES
  (10, 'cat1')
, (10, 'cat2')
, (10, 'cat3')
, (11, 'dog1')

;WITH x AS
(
	SELECT Id
	, aText
	, ROW_NUMBER() OVER (PARTITION BY aText ORDER BY Id) AS 'row'
	FROM @tmeow
), 
r AS
(
	SELECT a.Id, a.aText, a.row FROM x a
	WHERE a.row = 1
	
	UNION ALL
	
	SELECT x.Id, r.aText + ', ' + .aText, x.row
	FROM x
		INNER JOIN r ON x.Id = r.Id AND r.row = x.row + 1
	
)SELECT * FROM r


-- ----------------
-- Group Concatenation (first example)
-- ----------------
DECLARE @FamilyMemberPets TABLE (Name NVARCHAR(MAX), Pet NVARCHAR(MAX))
INSERT INTO @FamilyMemberPets(Name,Pet)VALUES
('Madeline','Kirby'),
('Madeline','Quigley'),
('Henry',   'Piglet'),
('Lisa',    'Snowball'),
('Lisa',    'Snowball II');

;WITH x as 
(
  SELECT Name
  , CONVERT(NVARCHAR(MAX), Pet) AS 'Pet'	-- list pets
  ,ROW_NUMBER() OVER (PARTITION BY Name ORDER BY Pet) AS 'r1' -- based on name
  FROM @FamilyMemberPets
),
a AS	-- select the first of each partition
(
  SELECT Name
  , Pet
  , r1 
  FROM x WHERE 
  r1 = 1
),
r AS
(
  SELECT Name, Pet, r1 FROM a WHERE r1 = 1	-- this is the same as SELECT * FROM a
  UNION ALL
  SELECT x.Name, r.Pet + ', ' + x.Pet, x.r1
    FROM x 
		INNER JOIN r ON x.Name = r.Name 
				    AND x.r1 = r.r1 + 1
), 
final AS
(
	SELECT Name, MAX(LEN(Pet)) AS 'Pet'
	FROM r 
	GROUP BY Name
)SELECT *
 FROM r 
	INNER JOIN final f ON r.Name = f.Name AND LEN(r.Pet) = f.Pet


-- ===============================
-- GROUP, UNION, DISTINCT, etc. | Aggregation
-- ===============================

-- ==============================
-- distinct
-- ==============================
DECLARE @table2 TABLE (Id INT, Name NVARCHAR(MAX), Value NVARCHAR(MAX)); INSERT INTO @table2 VALUES
  (1, 'MARTIN', NULL)
, (1, 'MARTIN QUITO', NULL)
, (1, 'MARTIN QUITO LLOCLLE AAAA', NULL)
, (1, 'MARTIN QUITO LLOCLLE Zasdasdsa', NULL)
, (2, 'NEFI', NULL)
, (2, 'NEFI QUITO', NULL)
, (3, NULL, NULL)
, (3, 'a', NULL)
, (4, 'b', 'a')

SELECT DISTINCT Name
FROM @table2
	-- . yes distinct, will return null

-- ===============================
-- REPLACE
-- ===============================
-- REPLACE ( string_expression , string_pattern , string_replacement )
SELECT REPLACE('martin quito', 'qu', '')


-- ===============================
-- compare nulls
-- ===============================
DECLARE @table6 TABLE (Id INT, Id2 INT, Id3 INT)
 INSERT INTO @table6 VALUES (1, NULL, 1), (2, NULL, 2), (3, 3, 3)
SELECT *
FROM @table6
WHERE Id <> Id2

-- ==========================
-- TMP PROCEDURES
-- ==========================

/* temp procedure */
GO
CREATE PROCEDURE #test AS
	SELECT 'HELLO'
GO

EXEC #test

DROP PROCEDURE #test


/* temp procedure with params */
GO
CREATE PROCEDURE #martin 
	  @aMoney INT
	, @data NVARCHAR(MAX)
AS
BEGIN
	SELECT 'a'
END
GO
EXEC #martin 2, 'Martin'
DROP PROCEDURE #martin

/* temp procedure with param that returns STRING */
GO 
CREATE PROCEDURE #mybestProc 
	  @firstName NVARCHAR(MAX)
	, @lastName NVARCHAR(MAX)
	, @fullName NVARCHAR(MAX) OUTPUT
AS
	SELECT @firstName + ' ' +  @lastName
	RETURN
	SELECT 'a'
GO

-- declare variable to receive the output value of the procedure
DECLARE @myFullName NVARCHAR(MAX);

-- Execute the procedure
EXECUTE #mybestProc 'Martin', 'Quito', @fullName = @myFullName OUTPUT;

-- Display value returned by the procedure
PRINT @myFullName

GO
DROP PROCEDURE #mybestProc

-- ==========================
-- HOW TEXT CONCATENATION WORKS
-- ==========================
DECLARE @courseOutcomes TABLE (CourseId INT, OutcomeText NVARCHAR(MAX), OrderNum INT)
INSERT INTO @courseOutcomes VALUES
  (1, 'Text1',   1)
, (1, 'Text2',  2)
, (1, 'Text3',  3)
, (2, 'Cat', 1)
, (2, 'Dog', 2)
, (2, 'Hourse', 3)

;WITH joinedOutcomes (CourseId, OutcomeText, OrderNum) AS
(
	SELECT CourseId, OutcomeText, OrderNum
	FROM @courseOutcomes
	WHERE CourseId = 1
	AND OrderNum = 1
)	SELECT *
	FROM joinedOutcomes jo	
		INNER JOIN @courseOutcomes co ON jo.CourseId = co.CourseId
									AND jo.OrderNum + 1 = co.OrderNum    


-- =============================
-- MAX
-- ============================
-- max with nulls
DECLARE @ints TABLE (Id INT)
INSERT INTO @ints VALUES
(1), (2), (3), (NULL)

SELECT MAX(Id)
FROM @ints
	-- its okay


-- in the beginning
DECLARE @ints2 TABLE (Id INT)
INSERT INTO @ints2 VALUES
 (NULL), (1), (2), (3)

SELECT MAX(Id)
FROM @ints2

	-- its okay

-- =======================
-- GO
-- =======================
/*
- its not a tsql command
- used to to send the stmts since the last GO to the server
*/

-- ======================
-- NO LOCK
-- ======================
/*

https://www.mssqltips.com/sqlservertip/2470/understanding-the-sql-server-nolock-hint/

- kill is to kill a session
- if the session has a table inside a transaction, and in another session that table is queried, then using no-lock will ignore the transaction and read as it is including the transaction. This is a dirty read. The issue with the NOLOCK is there "possibility of reading data that has been changed, but not yet committed to the database. If you are running reports and do not care if the data might be off then this is not an issue, but if you are creating transactions where the data needs to be in a consistent state you can see how the NOLOCK hint could return false data.."
SELECT *
FROM COurse 
WITH (NOLOCK)
- EXEC sp_lock

*/

-- ======================
-- PRINT
-- ======================
/*
- Print 8000 cahrs if non-unicode, 4000 if unicode
- "T-SQL scripts and stored procedures don't produce their PRINT statements and RAISERROR output in a way that keeps you informed of the code's progress."
	- https://www.mssqltips.com/sqlservertip/1660/using-the-nowait-option-with-the-sql-server-raiserror-statement/


*/

=======
-- ===============
-- EXISTS
-- ===============
-- specifies a subquery to test for the existence of rows

DECLARE @tmp TABLE (id INT);
INSERT INTO @tmp VALUES (2),(3);

IF(EXISTS (SELECT * FROM @tmp))
BEGIN
	SELECT * FROM @tmp;
END;
	-- if rows exists 
	
IF (NOT EXISTS (SELECT * FROM @tmp))
BEGIN 
	SELECT * FROM @tmp
END;
	-- if no row exists

-- ======= another use ========
DECLARE @a TABLE (id INT, name NVARCHAR(MAX))
INSERT INTO @a VALUES (1, 'Martin'), (2, 'Martin'), (3, 'Martin')

SELECT * 
FROM @a t
WHERE  EXISTS (SELECT 1
				  FROM @a d
				  WHERE t.id >d.id)
		-- 2 > 1, 3 > 1 those are true which will be in the top queyr result 

-- EXISTS mean that waht ever the query gives, that is waht we are going to get, NOT exist means what ever the query gives, those records will be removed from the final query

-- ===============
-- SELECT 1
-- ===============
DECLARE @tmp TABLE (id INT);
INSERT INTO @tmp VALUES (2),(3);

SELECT 1000 FROM @tmp
SELECT TOP 1 Id FROM @tmp
SELECT TOP 2 Id FROM @tmp

	-- . generates 1 (no column name) with 2 rows of 1000
	-- . This ouputs the number(or char) with the number of #rows of the seleted @tmp

-- doesn't work --
SELECT TOP * FROM @tmp

	
-- ==============
-- SIMPLE COMMANDS
-- ==============
PRINT 'aaaahhh!!!'
SET NOCOUNT ON;
	-- ON stops the message that shows the count of the number of rows affected by a Transact-SQL statement or stored procedure from being returned as part of the result set.

-- ==============
-- WITH
-- ==============
-- (1)with vs select
SELECT 'dummy' as 'column'

;WITH aWithStatement AS
(
	SELECT 'dummy' as 'column'
)SELECT * FROM aWithStatement
	-- . "conceptually, the with clause allows us give us names to predefined SELECT statements inside the context of a LARGER SELECT statement. We can then reference the NAMED SELECT statements later. Now consider this next sequence of SELECT statements. 
	-- . you have to query the named query after it has been defined	


	
-- (2)reference a named query any number of times
;WITH a1 AS
(
	SELECT 'dummy' AS 'column'
)SELECT * FROM a1, a1
	-- this doesn't work because i have to suffix the tablles
;WITH a1 AS
(
	SELECT 'dummy' AS 'column'
)SELECT * FROM a1 x, a1 y
	-- this works, we can reference a named query any number of times
	

-- (3)create any number of named queries
;WITH tmp1 AS
(
	SELECT 'hello' AS 'column', 'MartinQuito' AS 'column_name'
	UNION
	SELECT 'goodmorning', 'MiriamQuito'
	
), tmp2 AS
(
	SELECT 'bye' AS 'column1'
), tmp3 AS
(
	SELECT 'hi' AS 'column2'
) SELECT * FROM tmp1, tmp2, tmp3
	-- . works fine
	-- . at least one with needs to be called to not receive exceptions
	
-- (4)named queries can reference other named queries that came before them and even correlate to previous named queries
;WITH tmp1 AS
(
	SELECT 'hello' AS 'column'
), tmp2 AS
(
	SELECT 
	  (SELECT * FROM tmp1)	as 'x'
	, 'bye'					as 'y'
)SELECT * FROM tmp2
	-- . "named queries are only good for the SELECT statement that names them, their scope is local to the SELECT in which they are defined, hence no sharing across statements."
	
-- (5) with with insert
DECLARE @tmp TABLE (id INT, StudentName NVARCHAR(MAX));
;WITH select1 AS
(
	SELECT 1 AS 'Student Id'
	, 'Martin Quito' AS 'Student'
)
INSERT INTO @tmp
SELECT * FROM select1
SELECT * FROM @tmp -- query tmp table
	-- after your SELECT has been built, you can then inserted into a table
	
	
-- =============
-- ltrim AND rtrim
-- =============
-- ltrim. returns a character expression after it removes leading blanks
DECLARE @name NVARCHAR(MAX) = ' Martin '
SELECT LTRIM(@name), LEN(@name) AS 'before', LEN(LTRIM(@name)) as 'after';
	-- 'Martin '
	-- removes the spaces before
	
-- rtrim. returns a character string after truncating all trailing blanks
DECLARE @name NVARCHAR(MAX) = ' Martin '
SELECT RTRIM(@name), LEN(@name) AS 'before', LEN(RTRIM(@name)) as 'after';
	-- ' Martin'
	-- removes the spaces after

-- . to remove spaces, I'll have to first aplply the rtrim and then the ltrim





-- =============
-- JOINS
-- =============
-- LEFT JOIN. "returns all rows from the left table, even if there is no matches int he right table. This means that if the ON cluases matches 0 (zero) records in right table, the join will still return a row in the result, but with NULL in each column from right table. This means that a left join returns all the values from the left table, plus matched values from the right table OR NULL in case of no matching join predicate"
;WITH departments AS
(
	SELECT 1 as 'department_id', 'english' as 'department_name'
	UNION
	SELECT 2 as 'department_id', 'math' as 'department_name'
	UNION 
	SELECT 3 as 'department_id', 'biology' as 'department_name'
	UNION
	SELECT 4 as 'department_id', 'IT' as 'department_name'
), teachers AS
(
	SELECT 1 as 'teacher_id', 'Mr.Bob' as 'teacher_name', 1 as 'department_id'
	UNION
	SELECT 2 as 'teacher_id', 'Mr.Nancy' as 'teacher_name', NULL as 'department_id'
	UNION
	SELECT 3 as 'teacher_id', 'Mr.Peter' as 'teacher_name', 2 as 'department_id'
	UNION
	SELECT 4 as 'teacher_id', 'Mr.SpongeBob' as 'teacher_name', NULL as 'department_id'
)	SELECT * FROM teachers t
				FULL OUTER JOIN departments dp ON t.department_id = dp.department_id
	
	--. when doing a select UNION, the first SELECT column name takes precedence.
	--. INNER JOIN returns rows when there is a match in both tables.
	--. LEFT JOIN returns all rows from the left table, even if there are no matches in the right table.
	--. RIGHT JOIN returns all rows from the right table, even if there is no matches in the left table

-- =============
-- MERGE
-- =============

-- target. this represents the books that are or were available at a functional book retailer. if quantity is 0 then is sold out
DECLARE @BookInventory TABLE (TitleID INT NOT NULL PRIMARY KEY
							, Title NVARCHAR(100) NOT NULL
							, Quantity INT NOT NULL);
							
-- source. this represents books for which an order has been placed and delivered ( we adding more books to the inventory). if quantity value for a book listed in this table is 0, then the book had been requeted but not included with the delivery. The
DECLARE @BookOrder TABLE(	TitleID INT NOT NULL PRIMARY KEY
						,   Title NVARCHAR(100) NOT NULL
						,   Quantity INT NOT NULL);
						
INSERT INTO @BookInventory VALUES 
	(1, 'The Catcher in the Rye', 6),
	(2, 'Pride and Prejudice', 3),
	(3, 'The Great Gatsby', 0),
	(5, 'Jane Eyre', 0),
	(6, 'Catch 22', 0);

INSERT INTO @BookOrder VALUES
  (1, 'The Catcher in the Rye', 3),
  (3, 'The Great Gatsby', 0),
  (4, 'Gone with the Wind', 4),
  (5, 'Jane Eyre', 5),
  (7, 'Age of Innocence', 8);
  
-- WHEN MATCHED clause
-- . "The first MERGE clause we�ll look at is WHEN MATCHED. You should use this clause when you want to update or delete rows in the target table that match rows in the source table. Rows are considered matching when the joined column values are the same.For example, if the BookID value in the BookInventory table matches the BookID value in the BookOrder table, the rows are considered to match, regardless of the other values in the matching rows. When rows do match, you can use the WHEN MATCHED clause to modify data in the target table. Lets look at an example to demonstrate how this works."

-- WHEN NOT MATCHED cluase
-- . "The next clause in the MERGE statement we�ll review is WHEN NOT MATCHED [BY TARGET]. (The BY TARGET keywords are optional.) You should use this clause to insert new rows into the target table. The rows you insert into the table are those rows in the source table for which there are no matching rows in the target. For example, the BookOrder table contains a row for Gone with the Wind. However, the BookInventory table does not contain this book. The following example demonstrates how to include a WHEN NOT MATCHED clause in your MERGE statement that adds Gone with the Wind to your target table:"

-- what we are doing here is we are updating the inventory when the source and target matches using a column key
MERGE INTO @BookInventory bi -- target
	USING @BookOrder bo			 -- table source
	ON bi.TitleID = bo.TitleID -- merge_search_condiition
	
	WHEN MATCHED THEN	-- use this to update or delete
		UPDATE
		SET bi.Quantity = bi.Quantity + bo.Quantity
		
		--1	The Catcher in the Rye	9
		--2	Pride and Prejudice		3
		--3	The Great Gatsby		0
		--5	Jane Eyre				5
		--6	Catch 22				0
		-- . you can either update or delete
	
	WHEN NOT MATCHED BY TARGET THEN	-- rows you inserte into the table are those rows in the SOURCE table for which there were no matching rows in the targetr. When you have rows that are in the source but not in target, insert the source. When NOT matched by target means "when rows not found in target" then insert rows from the sourec.
		INSERT (TitleID, Title, Quantity)
		VALUES (bo.TitleID, bo.Title, bo.Quantity)
		
		--1	The Catcher in the Rye	9
		--2	Pride and Prejudice		3
		--3	The Great Gatsby		0
		--4	Gone with the Wind		4	-- INSERTED
		--5	Jane Eyre				5
		--6	Catch 22				0
		--7	Age of Innocence		8	-- INSERTED
	
	WHEN NOT MATCHED BY SOURCE -- delete rows from the target table that does not match a row in the source table. When you have data in the source table that has not relationship in the target...then delete the target data.  
		AND bi.Quantity = 0 THEN
		DELETE
		--1	The Catcher in the Rye	9
		--2	Pride and Prejudice		3
		--3	The Great Gatsby		0
		--4	Gone with the Wind		4
		--5	Jane Eyre				5
		--7	Age of Innocence		8
		-- (Catch 22) was deleted

	OUTPUT
		$action, DELETED.*, INSERTED.*;
	
SELECT * FROM @BookInventory




-- union: ditinct
-- union all: allows duplicatives


-- =============
-- CHARINDEX
-- =============
-- Searches an expression for another expression and returns its starting position if found.
-- CHARINDEX ( expressionToFind ,expressionToSearch [ , start_location ] ) 

DECLARE @document varchar(64);
SELECT @document = 'martin quito';
SELECT CHARINDEX('quito', @document);
GO
	-- 8
	-- starts with index 1

DECLARE @phrase NVARCHAR(MAX) = 'martin quito lloclle'
SELECT CHARINDEX('=', @phrase)

-- =============
-- PATINDEX
-- =============
/*
The CHARINDEX and PATINDEX functions return the starting position of a pattern you specify. PATINDEX can use wildcard characters, but CHARINDEX cannot.
*/
DECLARE @document2 varchar(64);
SELECT @document2 = 'martin quito';

-- SELECT PATINDEX(PatternChar, ExpressionChar)
SELECT PATINDEX('%q%', @document2)

-- =============
-- LEFT AND RIGHT
-- =============
-- returns the left part of a character string with the specified number of characters
-- index starts at 1

-- LEFT ( character_expression , integer_expression )
DECLARE @Name NVARCHAR(60) = 'Martin Quito';
SELECT LEFT(@name, 3);
	-- subtrings the first two characters (left side)

-- RIGHT ( character_expression , integer_expression )
DECLARE @Name1 NVARCHAR(60) = 'Martin Quito';
SELECT RIGHT(@name1, 3);
	-- subtrings the last three characters (right side)	


-- ===========
-- CONVERT
-- ===========
-- converts an expression of one data type to another in SQL Server 2016.
-- Syntax for CONVERT:
-- CONVERT ( data_type [ ( length ) ] , expression [ , style ] )
DECLARE @int INT = '2';
SELECT CONVERT(INT, @int);

/* 
CAST vs CONVERT
- cast is more standard, but limited. Convert is like an enhanced type of cast. It is T-SQL standard
*/


-- =========
-- STUFF
-- =========
-- The STUFF function inserts a string into another string. It deletes a specified length of characters in the first string at the start position and then inserts the second string into the first string at the start position
-- STUFF ( character_expression , start , length , replaceWith_expression )
	-- character_expression is the base string
	-- replaceWith_expression is the substring you want to insert
SELECT STUFF('abcdef', 2, 3, 'martin');
	-- abcdef - base string
	-- 2 - where it starts
	-- 3 - length starting from 2 (bcd)
	-- martin - what to be inserted

-- ==========
-- STRIPPING OUT values
-- ==========
SELECT 1
WHERE 'ENG/ENGR' = 'ENGR' 
OR ( 'ENG/ENGR' LIKE 'ENGR/%')
OR ( 'ENG/ENGR' LIKE '%/ENGR')
OR ( 'ENG/ENGR' LIKE '%/ENGR/%')

;WITH tmp AS
(
	SELECT 'Martin, Quito' AS 'Name'
)SELECT *
 FROM tmp t
	INNER JOIN tmp o ON t.Name = 


-- ========
-- search all tables
-- ========
EXEC sys.sp_MSforeachtable 'SELECT * FROM ?'


-- ========
-- nullif
-- ========
-- return NULL if 2 params are equal
-- NULL IF!!!!!!! ... 
SELECT NULLIF(2,2)
	-- NULL
	-- .if both are the same, return NULL

SELECT NULLIF('3', '2')
	-- 3
	-- .if different, return the first expression
	
SELECT 2 / NULLIF(NULL, 0)
	-- ERROR  "The type of the first argument to NULLIF cannot be the NULL constant because the type of the first argument has to be known."

SELECT 2 / NULLIF(0 , 1)
	-- ERROR "Divide by zero error encountered."

SELECT 2 / NULLIF(1, 0)
	-- 2
	
	
SELECT 2/NULL
	-- NULL
SELECT 2/0
	-- ERROR


-- =====================
-- Concatenate strings
-- =====================
CREATE TABLE #YourTable ([ID] INT, [Name] NVARCHAR(MAX), [Value] INT)

INSERT INTO #YourTable ([ID],[Name],[Value]) VALUES (1,'Aaaaa',4)
INSERT INTO #YourTable ([ID],[Name],[Value]) VALUES (1,'Bdasdasdasd',8)
INSERT INTO #YourTable ([ID],[Name],[Value]) VALUES (1,'Casdasdas',8)
INSERT INTO #YourTable ([ID],[Name],[Value]) VALUES (2,'Casdasdas',9)

SELECT [ID]
,  STUFF(
			   (SELECT  N', ' + [Name] + N':' + CAST([Value] AS VARCHAR(MAX)) 
				FROM #YourTable 
				WHERE (ID = Results.ID) 
				FOR XML PATH(''),TYPE).value(N'(./text())[1]',N'NVARCHAR(MAX)') -- xml path allows to return more than one record for the subquery which then retti
  ,1,2,'') 
FROM #YourTable Results
GROUP BY ID

DROP TABLE #YourTable
-- ===================
-- FOR XML
-- ===================
-- A SELECT query returns results as a rowset. You can optionally retrieve formal results of a SQL query as XML by specifying the FOR XML clause in the quer
SELECT 'abbb'
FOR XML PATH;
	-- <row>abbb</row>
	
SELECT 'abbb'
FOR XML PATH('')
	-- abbb
	
SELECT (SELECT 'abbb' FOR XML PATH(''), TYPE)
	-- abbb
	-- . returns with no column name 
	
SELECT (SELECT 'abbb' FOR XML PATH(''), TYPE).value('(./text())[1]', 'NVARCHAR(MAX)')
	-- . turns the abbb into xml and then retrieves the value 
	-- .value(XQUERY, SQLTYPE)
	-- .value extracts a value from an XML instance
	

-- ========
-- finding . 
-- ========
SELECT (CASE
	WHEN '320109' NOT LIKE '%.%' THEN STUFF('320109', 3, 0, '.')
END)

SELECT (CASE
	WHEN '320109' NOT LIKE '%.%' THEN STUFF('320109', 3, 0, '.')
END)


-- ====================
-- where 
-- ====================
DECLARE @table TABLE (id INT, firstName NVARCHAR(MAX), lastName NVARCHAR(MAX))
INSERT INTO @table
VALUES
(1, 'Martin', 'Quito')
, (2, 'Martin', 'Quito')
, (3, 'Miriam', 'Quito')
, (4, 'Nefi', 'Quito')

SELECT * 
FROM @table
WHERE Id <> 4
AND firstName <> 'Nefi'
	-- my prediction is Id: 1, 2, 3
	-- 1, 2, 3


-- ====================
-- remove last perioid
-- ====================
SELECT STUFF('Participation.',  LEN('Participation.'), 1,'')




-- ==================
-- Change title first letter to be capitalized and other lower case
-- ==================
DECLARE @Index          INT
DECLARE @Char           CHAR(1)
DECLARE @PrevChar       CHAR(1)
DECLARE @OutputString   VARCHAR(255)
DECLARE @InputString NVARCHAR(MAX) = 'INTRODUCTION''S TO !CULTURAL ANTHROPOLOGY'

SET @OutputString = LOWER(@InputString)
SET @Index = 1

WHILE @Index <= LEN(@InputString)
BEGIN
    SET @Char     = SUBSTRING(@InputString, @Index, 1)
    SET @PrevChar = CASE WHEN @Index = 1 THEN ' '
                         ELSE SUBSTRING(@InputString, @Index - 1, 1)
                    END
    -- if prev has the following then make the next char UPPER
    IF @PrevChar IN (' ', ';', ':', '!', '?', ',', '.', '_', '-', '/', '&', '''', '(')
    BEGIN
		-- excep that if the prev has ' and the following has s, then its lower case
        IF (@PrevChar != '''' OR UPPER(@Char) != 'S')
			BEGIN SET @OutputString = STUFF(@OutputString, @Index, 1, UPPER(@Char)); END
    END
    SET @Index = @Index + 1
END


-- =====================
-- SUBSTRING
-- =====================
-- removes a string from a string by recibing the index and length
DECLARE @a NVARCHAR(MAX) = 'Martin'
SELECT SUBSTRING(@a, 1, 1)
	-- 'M'
	-- .parameters(Text, start, length)

DECLARE @b NVARCHAR(MAX) = 'Martin'
SELECT SUBSTRING(@b, 1)
	-- Error: requires 3 arguments

DECLARE @c NVARCHAR(MAX) = 'Martin'
SELECT SUBSTRING(@c, NULL, NULL)
	-- NULL
	
-- =================
-- ROW_NUMBER
-- =================
-- Returns a sequential number of row within a partition of a result set, starting at 1 for the first row in each partition
-- parameters
	-- order_by clause | The ORDER BY by clause determines the sequence in which the rows are assigned their unique ROW_NUMBER within a specified parition. It is required.
	-- PARTITION BY value_expression | Divides the result set produced by the FROM clause into partitions to which the ROW_NUMBER function is applied. "value_expression" specifies the column by which the result is parition. If PARTITION BY is not specified, the function treats all rows of the query result set as a single group.


-- simple
;WITH tmp (id, name) AS
(
	SELECT 1, 'martin'
	UNION 
	SELECT 2, 'nefi'
	UNION
	SELECT 3, 'miriam'
)SELECT id, name, ROW_NUMBER() OVER (ORDER BY id)
 FROM tmp
 
 -- random ids
 ;WITH tmp (id, name) AS
(
	SELECT 12, 'martin'
	UNION 
	SELECT 100123, 'nefi'
	UNION
	SELECT 10, 'miriam'
)SELECT id, name, ROW_NUMBER() OVER (ORDER BY id)
 FROM tmp
	--. the ids are treated as text

 -- ==================================
 DECLARE @ad TABLE (Id INT, Name NVARCHAR(MAX))
 INSERT INTO @ad VALUES
  (12, 'martin')
 , (100123, 'nefi')
 , (1, 'miriam')
 SELECT Id, Name, ROW_NUMBER() OVER (ORDER BY Id)
 FROM @ad 
	-- .the order of the id is based on the Id. 

-- ==================================
-- updating the sort-order
DECLARE @df TABLE (Id INT, Name NVARCHAR(MAX), SortOrder INT)
INSERT INTO @df VALUES
 	 (1, 'Martin', NULL)
	,(2, 'Nefi', NULL)
	,(3, 'Miriam', NULL)

--UPDATE @df
--SET SortOrder = (ROW_NUMBER() OVER (ORDER BY Id))
	-- window functions can only appear in the SELECT or ORDER BY clauses
	
UPDATE  t
SET SortOrder = a.rowNum
FROM @df t
	INNER JOIN (SELECT Id, (ROW_NUMBER() OVER (ORDER BY Id)) as 'rowNum' FROM @df) a ON t.Id = a.Id
	
SELECT * FROM @df


-- using the partition
DECLARE @aTable TABLE (Id INT, Name NVARCHAR(MAX), Gender NVARCHAR(MAX), SortOrder INT)
INSERT INTO @aTable VALUES
 	 (1, 'Martin', 'Male',NULL)
	,(2, 'Nefi', 'Male',NULL)
	,(3, 'Miriam', 'Female', NULL)
SELECT Id, Name, Gender, (ROW_NUMBER() OVER (PARTITION BY Gender ORDER BY Id))
FROM @aTable 
ORDER BY Id



-- ==========================
-- Recursive
-- ==========================
-- the most simplest recursive CTE
;WITH countMe (Number) AS
(
	SELECT 1				-- anchor
	UNION ALL
	
	SELECT Number +1
	FROM countMe
	WHERE Number + 1 <= 3	-- recursive member
)SELECT * 
 FROM countMe
 
 -- factorial
 ;WITH fac(n, n1) AS
 (
	SELECT 1, 1
	UNION ALL
	
	SELECT n + 1, n + 1
	FROM fac
	WHERE n + 1 < 99
	
 )SELECT * 
  FROM fac

-- ---------
-- first try to do grouped concatenation 
-- ---------
DECLARE @tmeow TABLE (Id INT, aText NVARCHAR(MAX))
INSERT INTO @tmeow VALUES
  (10, 'cat1')
, (10, 'cat2')
, (10, 'cat3')
, (11, 'dog1')

;WITH x AS
(
	SELECT Id
	, aText
	, ROW_NUMBER() OVER (PARTITION BY aText ORDER BY Id) AS 'row'
	FROM @tmeow
), 
r AS
(
	SELECT a.Id, a.aText, a.row FROM x a
	WHERE a.row = 1
	
	UNION ALL
	
	SELECT x.Id, r.aText + ', ' + .aText, x.row
	FROM x
		INNER JOIN r ON x.Id = r.Id AND r.row = x.row + 1
	
)SELECT * FROM r


-- ----------------
-- Group Concatenation (first example)
-- ----------------
DECLARE @FamilyMemberPets TABLE (Name NVARCHAR(MAX), Pet NVARCHAR(MAX))
INSERT INTO @FamilyMemberPets(Name,Pet)VALUES
('Madeline','Kirby'),
('Madeline','Quigley'),
('Henry',   'Piglet'),
('Lisa',    'Snowball'),
('Lisa',    'Snowball II');

;WITH x as 
(
  SELECT Name
  , CONVERT(NVARCHAR(MAX), Pet) AS 'Pet'	-- list pets
  ,ROW_NUMBER() OVER (PARTITION BY Name ORDER BY Pet) AS 'r1' -- based on name
  FROM @FamilyMemberPets
),
a AS	-- select the first of each partition
(
  SELECT Name
  , Pet
  , r1 
  FROM x WHERE 
  r1 = 1
),
r AS
(
  SELECT Name, Pet, r1 FROM a WHERE r1 = 1	-- this is the same as SELECT * FROM a
  UNION ALL
  SELECT x.Name, r.Pet + ', ' + x.Pet, x.r1
    FROM x 
		INNER JOIN r ON x.Name = r.Name 
				    AND x.r1 = r.r1 + 1
), 
final AS
(
	SELECT Name, MAX(LEN(Pet)) AS 'Pet'
	FROM r 
	GROUP BY Name
)SELECT *
 FROM r 
	INNER JOIN final f ON r.Name = f.Name AND LEN(r.Pet) = f.Pet


-- ===============================
-- GROUP, UNION, DISTINCT, etc. | Aggregation
-- ===============================

-- ==============================
-- distinct
-- ==============================
DECLARE @table2 TABLE (Id INT, Name NVARCHAR(MAX), Value NVARCHAR(MAX)); INSERT INTO @table2 VALUES
  (1, 'MARTIN', NULL)
, (1, 'MARTIN QUITO', NULL)
, (1, 'MARTIN QUITO LLOCLLE AAAA', NULL)
, (1, 'MARTIN QUITO LLOCLLE Zasdasdsa', NULL)
, (2, 'NEFI', NULL)
, (2, 'NEFI QUITO', NULL)
, (3, NULL, NULL)
, (3, 'a', NULL)
, (4, 'b', 'a')

SELECT DISTINCT Name
FROM @table2
	-- . yes distinct, will return null

-- ===============================
-- REPLACE
-- ===============================
-- REPLACE ( string_expression , string_pattern , string_replacement )
SELECT REPLACE('martin quito', 'qu', '')


-- ===============================
-- compare nulls
-- ===============================
DECLARE @table6 TABLE (Id INT, Id2 INT, Id3 INT)
 INSERT INTO @table6 VALUES (1, NULL, 1), (2, NULL, 2), (3, 3, 3)
SELECT *
FROM @table6
WHERE Id <> Id2

-- ==========================
-- TMP PROCEDURES
-- ==========================

/* temp procedure */
GO
CREATE PROCEDURE #test AS
	SELECT 'HELLO'
GO

EXEC #test

DROP PROCEDURE #test


/* temp procedure with params */
GO
CREATE PROCEDURE #martin 
	  @aMoney INT
	, @data NVARCHAR(MAX)
AS
BEGIN
	SELECT 'a'
END
GO
EXEC #martin 2, 'Martin'
DROP PROCEDURE #martin

/* temp procedure with param that returns STRING */
GO 
CREATE PROCEDURE #mybestProc 
	  @firstName NVARCHAR(MAX)
	, @lastName NVARCHAR(MAX)
	, @fullName NVARCHAR(MAX) OUTPUT
AS
	SELECT @firstName + ' ' +  @lastName
	RETURN
	SELECT 'a'
GO

-- declare variable to receive the output value of the procedure
DECLARE @myFullName NVARCHAR(MAX);

-- Execute the procedure
EXECUTE #mybestProc 'Martin', 'Quito', @fullName = @myFullName OUTPUT;

-- Display value returned by the procedure
PRINT @myFullName

GO
DROP PROCEDURE #mybestProc

-- ==========================
-- HOW TEXT CONCATENATION WORKS
-- ==========================
DECLARE @courseOutcomes TABLE (CourseId INT, OutcomeText NVARCHAR(MAX), OrderNum INT)
INSERT INTO @courseOutcomes VALUES
  (1, 'Text1',   1)
, (1, 'Text2',  2)
, (1, 'Text3',  3)
, (2, 'Cat', 1)
, (2, 'Dog', 2)
, (2, 'Hourse', 3)

;WITH joinedOutcomes (CourseId, OutcomeText, OrderNum) AS
(
	SELECT CourseId, OutcomeText, OrderNum
	FROM @courseOutcomes
	WHERE CourseId = 1
	AND OrderNum = 1
)	SELECT *
	FROM joinedOutcomes jo	
		INNER JOIN @courseOutcomes co ON jo.CourseId = co.CourseId
									AND jo.OrderNum + 1 = co.OrderNum    


-- =============================
-- MAX
-- ============================
-- max with nulls
DECLARE @ints TABLE (Id INT)
INSERT INTO @ints VALUES
(1), (2), (3), (NULL)

SELECT MAX(Id)
FROM @ints
	-- its okay


-- in the beginning
DECLARE @ints2 TABLE (Id INT)
INSERT INTO @ints2 VALUES
 (NULL), (1), (2), (3)

SELECT MAX(Id)
FROM @ints2

	-- its okay

>>>>>>> Stashed changes
