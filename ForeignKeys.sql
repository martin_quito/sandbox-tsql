<<<<<<< Updated upstream
/*
The Importance of these 3 imporatnt sys procs of foreign keys. SPCSNYU is used. 
*/
USE IDOE

-- =========================================
-- sys.foreign_key_columns
-- =========================================
/*
sys.foreign_key_columns
- "Contains a row for each column, or set of columns, that comprise a foreign key

constraint_object_id	int		ID of the FOREIGN KEY constraint. | foreign key object
constraint_column_id	int		ID of the column, or set of columns, that comprise the FOREIGN KEY (1..n where n=number of columns). | number of columns where fk object is applied
parent_object_id		int		ID of the parent of the constraint, which is the referencing object. | table where the fk object lives and is the table that REFERENCES to another table (object)
parent_column_id		int		ID of the parent column, which is the referencing column. | the column that has the constraint object applied, you can call this the referencing column becaus eit references to another table (that table is the referecned object)
referenced_object_id	int		ID of the referenced object, which has the candidate key.
referenced_column_id	int		ID of the referenced column (candidate key column).

*/
SELECT 
  o3.name	AS ContraintObject
, s.name    AS [Schema]
, o.name	AS ReferencingObject
, c.name	AS ReferencingColumn
, o2.name	AS ReferencedObject
, c2.name	AS ReferencedColumn
FROM sys.foreign_key_columns fkc
	INNER JOIN sys.objects o3	ON fkc.constraint_object_id = o3.object_id
	INNER JOIN sys.objects o	ON fkc.parent_object_id = o.object_id 
	INNER JOIN sys.schemas s	ON o.schema_id = s.schema_id
	INNER JOIN sys.columns c    ON fkc.parent_column_id = c.column_id
								AND o.object_id = c.object_id
	INNER JOIN sys.objects o2	ON fkc.referenced_object_id = o2.object_id 
	INNER JOIN sys.columns c2	ON fkc.referenced_column_id = c2.column_id 
								AND o2.object_id = c2.object_id 
WHERE o2.name = 'MetaSelectedField'


SELECT *
FROM sys.foreign_key_columns

SELECT s2.name AS [ReferencedSchema]
, o2.name	AS ReferencedObject
, c2.name	AS ReferencedColumn
, s.name    AS [ReferencingSchema]
, o.name	AS ReferencingObject
, c.name	AS ReferencingColumn
, '(SELECT 1 FROM ' + o.name + ' a2 WHERE a2.' + c.name + ' = a1.' +  c2.name + ')' AS WhereClause
FROM sys.foreign_key_columns fkc
	INNER JOIN sys.objects o3	ON fkc.constraint_object_id = o3.object_id
	INNER JOIN sys.objects o	ON fkc.parent_object_id = o.object_id 
	INNER JOIN sys.schemas s	ON o.schema_id = s.schema_id
	INNER JOIN sys.columns c    ON fkc.parent_column_id = c.column_id
								AND o.object_id = c.object_id
	INNER JOIN sys.objects o2	ON fkc.referenced_object_id = o2.object_id 
	INNER JOIN sys.schemas s2   ON o2.schema_id = s2.schema_id
	INNER JOIN sys.columns c2	ON fkc.referenced_column_id = c2.column_id 
								AND o2.object_id = c2.object_id 
WHERE o2.name = 'DEPARTMENTS'


-- =========================================
-- sys.foreign_keys
-- =========================================
/*
sys.sysforeignkeys
- contains finromation aboout the foreign contrains that are in the definitions of table in the database

constid		| INT | ID of the FOREIGN KEY constraint
fkeyid		| INT | Object ID of the table with the FOREIGN KEY constraint
rkeyId		| INT | Object ID of the table refereced in the FOREIGN KEY constraint
fkey		| smallint | ID of the referencing column
rkey		| smallint | ID of the referenced column
keyno		| smallint | Position of the column in the reference column list
*/
SELECT *
FROM sys.foreign_keys

-- =========================================
-- sys.sysforeignkeys
-- =========================================
SELECT *
FROM sys.sysforeignkeys
ORDER BY fkeyid



-- ===================================
-- ALTERING A TABLE
-- ===================================
/*
Course(*) - StatusAlias(1)

ALTER TABLE [dbo].[Course]  WITH NOCHECK 
	ADD  CONSTRAINT [FK_Course_StatusAliasId] 
	FOREIGN KEY([StatusAliasId])
	REFERENCES [dbo].[StatusAlias] ([Id])

Points from Course to status alias (parent to child)


*/

-- ===================================
-- CASCADE
-- ===================================
/*
- "Referential integrity is a relational database concept in which multiple tables share a relationship based on the data stored in the tables, and that relationship must remain consistent."

DELETE CASCADE
UPDATE CASCADE

References
-- https://www.mssqltips.com/sqlservertip/2743/using-delete-cascade-option-for-foreign-keys/

*/

-- DELETE CASCADE
-- parent table
CREATE TABLE [dbo].[Order](
 [OrderID] [bigint] NOT NULL,
 [OrderData] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Order_1] PRIMARY KEY CLUSTERED 
    ([OrderID] ASC)
)

-- child table
CREATE TABLE [dbo].[OrderDetail](
 [OrderDetailID] [bigint] NOT NULL,
 [OrderID] [bigint] NULL,
 [OrderData] [varchar](10) NULL,
 CONSTRAINT [PK_OrderDetail] PRIMARY KEY CLUSTERED 
    ([OrderDetailID] ASC)
)

-- foreign key constraint
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK 
ADD 
	CONSTRAINT [FK_OrderDetail_Order] 
	FOREIGN KEY([OrderID])
	REFERENCES [dbo].[Order] ([OrderID])
	ON DELETE CASCADE
	-- 

-- data load
DECLARE @val BIGINT
DECLARE @val2 BIGINT
SELECT @val=1
WHILE @val < 100
BEGIN  
   INSERT INTO dbo.[Order] VALUES(@val,'TEST' + CAST(@val AS VARCHAR))
   
   SELECT @val2=1
   WHILE @val2 < 20
   BEGIN  
      INSERT INTO dbo.[OrderDetail] VALUES ((@val*100)+@val2,@val,'TEST' + CAST(@val AS VARCHAR))
      SELECT @val2=@val2+1
   END
   SELECT @val=@val+1
     
END
GO 


DBCC DROPCLEANBUFFERS
DELETE FROM [Order] WHERE OrderID=55
	-- deleting from the parent 
	-- this also deleted the orderId 55 in the order_detail. Remeber that the fk was applied in the table that had the fk (child) and NOT IN the parent
	-- there was no warning!

-- what happens if the "ON DELETE CASACDE" is removed in the child table and recreate the constraint without the cascade
ALTER TABLE [dbo].[OrderDetail] DROP CONSTRAINT [FK_OrderDetail_Order]
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK 
ADD CONSTRAINT [FK_OrderDetail_Order] FOREIGN KEY([OrderID])
REFERENCES [dbo].[Order] ([OrderID])

DBCC DROPCLEANBUFFERS
DELETE FROM [Order] WHERE OrderID=22
	-- it throws an exception "The DELETE statement conflicted with the REFERENCE constraint "FK_OrderDetail_Order". The conflict occurred in database "master", table "dbo.OrderDetail", column 'OrderID'."
DELETE FROM [OrderDetail] WHERE OrderID=23
	-- .this needs to be ran first.

/*
- using the sql profier (the site states), it is much faster to use the cascade. 
- adding an index to the fk speed up the process and the performance is about the same thing in both methods. The nice thing about "ON DELETE CASCADE" is keeping the code much cleaner
- when adding the casacde constraint in the fk (child) ensures that if the parent (where the fk is referncing), if a record is deleted, then those values will get deleted in the child too.
*/


/*
ERRORS
Introducing FOREIGN KEY constraint 'CORE_ASSESSMENTS_FK1' on table 'CORE_ASSESSMENTS' may cause cycles or multiple cascade paths. Specify ON DELETE NO ACTION or ON UPDATE NO ACTION, or modify other FOREIGN KEY constraints.
- multiple casacde paths means that a record in this table can get delted from multiple places 
*/
-- lets try by a child habing two parents
CREATE TABLE StatusType
(
	Id INT NOT NULL
	, Name NVARCHAR(10) NOT NULL
	, CONSTRAINT [PK_Id1] PRIMARY KEY CLUSTERED 
    (Id ASC)
)
CREATE TABLE ProposalAlias
(
	Id INT NOT NULL
	, Name NVARCHAR(10) NOT NULL
	, CONSTRAINT [PK_Id2] PRIMARY KEY CLUSTERED 
    (Id ASC)
)
CREATE TABLE Courses 
(
	Id INT NOT NULL
	, StatusTypeId INT NOT NULL
	, ProposalAliasId INT NOT NULL
	, CONSTRAINT [PK_Id3] PRIMARY KEY CLUSTERED 
    (Id ASC)
)


ALTER TABLE Courses WITH CHECK
	ADD CONSTRAINT FK_Courses_StatusAlias 
		FOREIGN KEY (StatusTypeId)
		REFERENCES [StatusType] ([Id])
		ON DELETE CASCADE
ALTER TABLE Courses WITH CHECK
	ADD CONSTRAINT FK_Courses_ProposalAlias
		FOREIGN KEY (ProposalAliasId)
		REFERENCES ProposalAlias (Id)
		ON DELETE CASCADE
INSERT INTO StatusType VALUES
(1, 'Active')
, (2, 'Historical')

INSERT INTO ProposalAlias VALUES
(1, 'New')
, (2, 'Modify')


INSERT INTO Courses VALUES
(1, 1, 1)
, (2, 1, 2)
, (3, 2, 1)
, (4, 2, 2)


-- delete parent status typeId, what happens?
DELETE FROM StatusType
WHERE Id = 1
	-- . it actually deleted the two records in the courses that had 1 for status type. This means that a table can have multiple ON DELETE on different columns. Multiple cascade then means there are more than one cascade path coming from different ables to this table where those different tables are referenced by one table. 

/*
Multiple cascade pahts
*/
-- LISTING 2: Schema Creation Script for the 
-- Customers and Invoices Tables

IF OBJECT_ID('Invoices') IS NOT NULL DROP TABLE Invoices
IF OBJECT_ID('Customers') IS NOT NULL DROP TABLE Customers

CREATE TABLE Customers
(   customerid  char(5)  NOT NULL,
  /* other columns */
  CONSTRAINT PK_Customers PRIMARY KEY(customerid) )

CREATE TABLE Invoices
(   invoiceno int NOT NULL IDENTITY,
  source_customer char(5) NOT NULL,
  target_customer char(5) NOT NULL,
  /* other columns */
  CONSTRAINT PK_Invoices PRIMARY KEY(invoiceno) )

SET NOCOUNT ON

INSERT INTO Customers VALUES('FRODO')
INSERT INTO Customers VALUES('BILBO')
INSERT INTO Customers VALUES('GNDLF')

INSERT INTO Invoices VALUES('FRODO', 'BILBO')
INSERT INTO Invoices VALUES('FRODO', 'GNDLF')
INSERT INTO Invoices VALUES('BILBO', 'GNDLF')
INSERT INTO Invoices VALUES('BILBO', 'FRODO')
INSERT INTO Invoices VALUES('GNDLF', 'FRODO')
INSERT INTO Invoices VALUES('GNDLF', 'BILBO')

--LISTING 3: Code That Tries to Create Two Foreign Keys

ALTER TABLE Invoices ADD
  CONSTRAINT FK_Invoices_Customers_source
    FOREIGN KEY(source_customer)
    REFERENCES Customers(customerid)
      ON DELETE CASCADE
      ON UPDATE NO ACTION

ALTER TABLE Invoices ADD
  CONSTRAINT FK_Invoices_Customers_target
    FOREIGN KEY(target_customer)
    REFERENCES Customers(customerid)
      ON DELETE CASCADE
      ON UPDATE NO ACTION

	-- "Introducing FOREIGN KEY constraint 'FK_Invoices_Customers_target' on table 'Invoices' may cause cycles or multiple cascade paths. Specify ON DELETE NO ACTION or ON UPDATE NO ACTION, or modify other FOREIGN KEY constraints."
	-- . what this means is that a record that gets deleted multiple times from a delete trigger from the referencing table causes an error!!!

/*
Infinite Cycle
*/
CREATE TABLE CoreAssessments (
	Id INT NOT NULL
	, CoreAssessmentsId INT
	CONSTRAINT PK_CORE_ASSESSMENTS PRIMARY KEY (Id)
)

ALTER TABLE CoreAssessments ADD
	CONSTRAINT FK_CoreAssessments_CoreAssessments
		FOREIGN KEY (CoreAssessmentsId)
		REFERENCES CoreAssessments(Id)
			--ON DELETE CASCADE
			ON DELETE SET NULL;

	-- Introducing FOREIGN KEY constraint 'FK_CoreAssessments_CoreAssessments' on table 'CoreAssessments' may cause cycles or multiple cascade paths. Specify ON DELETE NO ACTION or ON UPDATE NO ACTION, or modify other FOREIGN KEY constraints.
	-- . it doesn't work both in "On delete casacde and on delete set null"

/*


*/


-- ==============
-- OTHER INFO
-- ==============
/*
WITH CHECK
	- default is "WITH CHECK", if "WITH CHECK" then it will validate the current data with the new constraint. Don't use "WITH NO CHECK"
DBCC DROPCLEANBUFFERS
	- removes all clean buffers from the buffer pool, and columnstore objects from the colujmnstore object pool. "we added a "DBCC DROPCLEANBUFFERS" to each DML query to ensure there is no data in the cache before we run the statement."
	- https://www.mssqltips.com/sqlservertip/1360/clearing-cache-for-sql-server-performance-testing/
=======
/*
The Importance of these 3 imporatnt sys procs of foreign keys. SPCSNYU is used. 
*/
USE SPCSNYU

/*
sys.foreign_key_columns
- "Contains a row for each column, or set of columns, that comprise a foreign key

constraint_object_id	int		ID of the FOREIGN KEY constraint. | foreign key object
constraint_column_id	int		ID of the column, or set of columns, that comprise the FOREIGN KEY (1..n where n=number of columns). | number of columns where fk object is applied
parent_object_id		int		ID of the parent of the constraint, which is the referencing object. | table where the fk object lives and is the table that REFERENCES to another table (object)
parent_column_id		int		ID of the parent column, which is the referencing column. | the column that has the constraint object applied, you can call this the referencing column becaus eit references to another table (that table is the referecned object)
referenced_object_id	int		ID of the referenced object, which has the candidate key.
referenced_column_id	int		ID of the referenced column (candidate key column).

*/
SELECT 
  o3.name	AS ContraintObject
, s.name    AS [Schema]
, o.name	AS ReferencingObject
, c.name	AS ReferencingColumn
, o2.name	AS ReferencedObject
, c2.name	AS ReferencedColumn
FROM sys.foreign_key_columns fkc
	INNER JOIN sys.objects o3	ON fkc.constraint_object_id = o3.object_id
	INNER JOIN sys.objects o	ON fkc.parent_object_id = o.object_id 
	INNER JOIN sys.schemas s	ON o.schema_id = s.schema_id
	INNER JOIN sys.columns c    ON fkc.parent_column_id = c.column_id
								AND o.object_id = c.object_id
	INNER JOIN sys.objects o2	ON fkc.referenced_object_id = o2.object_id 
	INNER JOIN sys.columns c2	ON fkc.referenced_column_id = c2.column_id 
								AND o2.object_id = c2.object_id 
WHERE o2.name = 'MetaSelectedField'

/*

SELECT *
FROM sys.foreign_keys

*/



/*
sys.sysforeignkeys
- contains finromation aboout the foreign contrains that are in the definitions of table in the database

constid		| INT | ID of the FOREIGN KEY constraint
fkeyid		| INT | Object ID of the table with the FOREIGN KEY constraint
rkeyId		| INT | Object ID of the table refereced in the FOREIGN KEY constraint
fkey		| smallint | ID of the referencing column
rkey		| smallint | ID of the referenced column
keyno		| smallint | Position of the column in the reference column list
*/

SELECT *
FROM sys.sysforeignkeys
ORDER BY fkeyid
-- ===================================
-- ALTERING A TABLE
-- ===================================
/*
Course(*) - StatusAlias(1)

ALTER TABLE [dbo].[Course]  WITH NOCHECK 
	ADD  CONSTRAINT [FK_Course_StatusAliasId] 
	FOREIGN KEY([StatusAliasId])
	REFERENCES [dbo].[StatusAlias] ([Id])

Points from Course to status alias (parent to child)


*/

-- ===================================
-- CASCADE
-- ===================================
/*
- "Referential integrity is a relational database concept in which multiple tables share a relationship based on the data stored in the tables, and that relationship must remain consistent."

DELETE CASCADE
UPDATE CASCADE

References
-- https://www.mssqltips.com/sqlservertip/2743/using-delete-cascade-option-for-foreign-keys/

*/

-- DELETE CASCADE
-- parent table
CREATE TABLE [dbo].[Order](
 [OrderID] [bigint] NOT NULL,
 [OrderData] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Order_1] PRIMARY KEY CLUSTERED 
    ([OrderID] ASC)
)

-- child table
CREATE TABLE [dbo].[OrderDetail](
 [OrderDetailID] [bigint] NOT NULL,
 [OrderID] [bigint] NULL,
 [OrderData] [varchar](10) NULL,
 CONSTRAINT [PK_OrderDetail] PRIMARY KEY CLUSTERED 
    ([OrderDetailID] ASC)
)

-- foreign key constraint
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK 
ADD 
	CONSTRAINT [FK_OrderDetail_Order] 
	FOREIGN KEY([OrderID])
	REFERENCES [dbo].[Order] ([OrderID])
	ON DELETE CASCADE
	-- 

-- data load
DECLARE @val BIGINT
DECLARE @val2 BIGINT
SELECT @val=1
WHILE @val < 100
BEGIN  
   INSERT INTO dbo.[Order] VALUES(@val,'TEST' + CAST(@val AS VARCHAR))
   
   SELECT @val2=1
   WHILE @val2 < 20
   BEGIN  
      INSERT INTO dbo.[OrderDetail] VALUES ((@val*100)+@val2,@val,'TEST' + CAST(@val AS VARCHAR))
      SELECT @val2=@val2+1
   END
   SELECT @val=@val+1
     
END
GO 


DBCC DROPCLEANBUFFERS
DELETE FROM [Order] WHERE OrderID=55
	-- deleting from the parent 
	-- this also deleted the orderId 55 in the order_detail. Remeber that the fk was applied in the table that had the fk (child) and NOT IN the parent
	-- there was no warning!

-- what happens if the "ON DELETE CASACDE" is removed in the child table and recreate the constraint without the cascade
ALTER TABLE [dbo].[OrderDetail] DROP CONSTRAINT [FK_OrderDetail_Order]
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK 
ADD CONSTRAINT [FK_OrderDetail_Order] FOREIGN KEY([OrderID])
REFERENCES [dbo].[Order] ([OrderID])

DBCC DROPCLEANBUFFERS
DELETE FROM [Order] WHERE OrderID=22
	-- it throws an exception "The DELETE statement conflicted with the REFERENCE constraint "FK_OrderDetail_Order". The conflict occurred in database "master", table "dbo.OrderDetail", column 'OrderID'."
DELETE FROM [OrderDetail] WHERE OrderID=23
	-- .this needs to be ran first.

/*
- using the sql profier (the site states), it is much faster to use the cascade. 
- adding an index to the fk speed up the process and the performance is about the same thing in both methods. The nice thing about "ON DELETE CASCADE" is keeping the code much cleaner
- when adding the casacde constraint in the fk (child) ensures that if the parent (where the fk is referncing), if a record is deleted, then those values will get deleted in the child too.
*/


/*
ERRORS
Introducing FOREIGN KEY constraint 'CORE_ASSESSMENTS_FK1' on table 'CORE_ASSESSMENTS' may cause cycles or multiple cascade paths. Specify ON DELETE NO ACTION or ON UPDATE NO ACTION, or modify other FOREIGN KEY constraints.
- multiple casacde paths means that a record in this table can get delted from multiple places 
*/
-- lets try by a child habing two parents
CREATE TABLE StatusType
(
	Id INT NOT NULL
	, Name NVARCHAR(10) NOT NULL
	, CONSTRAINT [PK_Id1] PRIMARY KEY CLUSTERED 
    (Id ASC)
)
CREATE TABLE ProposalAlias
(
	Id INT NOT NULL
	, Name NVARCHAR(10) NOT NULL
	, CONSTRAINT [PK_Id2] PRIMARY KEY CLUSTERED 
    (Id ASC)
)
CREATE TABLE Courses 
(
	Id INT NOT NULL
	, StatusTypeId INT NOT NULL
	, ProposalAliasId INT NOT NULL
	, CONSTRAINT [PK_Id3] PRIMARY KEY CLUSTERED 
    (Id ASC)
)


ALTER TABLE Courses WITH CHECK
	ADD CONSTRAINT FK_Courses_StatusAlias 
		FOREIGN KEY (StatusTypeId)
		REFERENCES [StatusType] ([Id])
		ON DELETE CASCADE
ALTER TABLE Courses WITH CHECK
	ADD CONSTRAINT FK_Courses_ProposalAlias
		FOREIGN KEY (ProposalAliasId)
		REFERENCES ProposalAlias (Id)
		ON DELETE CASCADE
INSERT INTO StatusType VALUES
(1, 'Active')
, (2, 'Historical')

INSERT INTO ProposalAlias VALUES
(1, 'New')
, (2, 'Modify')


INSERT INTO Courses VALUES
(1, 1, 1)
, (2, 1, 2)
, (3, 2, 1)
, (4, 2, 2)


-- delete parent status typeId, what happens?
DELETE FROM StatusType
WHERE Id = 1
	-- . it actually deleted the two records in the courses that had 1 for status type. This means that a table can have multiple ON DELETE on different columns. Multiple cascade then means there are more than one cascade path coming from different ables to this table where those different tables are referenced by one table. 

/*
Multiple cascade pahts
*/
-- LISTING 2: Schema Creation Script for the 
-- Customers and Invoices Tables

IF OBJECT_ID('Invoices') IS NOT NULL DROP TABLE Invoices
IF OBJECT_ID('Customers') IS NOT NULL DROP TABLE Customers

CREATE TABLE Customers
(   customerid  char(5)  NOT NULL,
  /* other columns */
  CONSTRAINT PK_Customers PRIMARY KEY(customerid) )

CREATE TABLE Invoices
(   invoiceno int NOT NULL IDENTITY,
  source_customer char(5) NOT NULL,
  target_customer char(5) NOT NULL,
  /* other columns */
  CONSTRAINT PK_Invoices PRIMARY KEY(invoiceno) )

SET NOCOUNT ON

INSERT INTO Customers VALUES('FRODO')
INSERT INTO Customers VALUES('BILBO')
INSERT INTO Customers VALUES('GNDLF')

INSERT INTO Invoices VALUES('FRODO', 'BILBO')
INSERT INTO Invoices VALUES('FRODO', 'GNDLF')
INSERT INTO Invoices VALUES('BILBO', 'GNDLF')
INSERT INTO Invoices VALUES('BILBO', 'FRODO')
INSERT INTO Invoices VALUES('GNDLF', 'FRODO')
INSERT INTO Invoices VALUES('GNDLF', 'BILBO')

--LISTING 3: Code That Tries to Create Two Foreign Keys

ALTER TABLE Invoices ADD
  CONSTRAINT FK_Invoices_Customers_source
    FOREIGN KEY(source_customer)
    REFERENCES Customers(customerid)
      ON DELETE CASCADE
      ON UPDATE NO ACTION

ALTER TABLE Invoices ADD
  CONSTRAINT FK_Invoices_Customers_target
    FOREIGN KEY(target_customer)
    REFERENCES Customers(customerid)
      ON DELETE CASCADE
      ON UPDATE NO ACTION

	-- "Introducing FOREIGN KEY constraint 'FK_Invoices_Customers_target' on table 'Invoices' may cause cycles or multiple cascade paths. Specify ON DELETE NO ACTION or ON UPDATE NO ACTION, or modify other FOREIGN KEY constraints."
	-- . what this means is that a record that gets deleted multiple times from a delete trigger from the referencing table causes an error!!!

/*
Infinite Cycle
*/
CREATE TABLE CoreAssessments (
	Id INT NOT NULL
	, CoreAssessmentsId INT
	CONSTRAINT PK_CORE_ASSESSMENTS PRIMARY KEY (Id)
)

ALTER TABLE CoreAssessments ADD
	CONSTRAINT FK_CoreAssessments_CoreAssessments
		FOREIGN KEY (CoreAssessmentsId)
		REFERENCES CoreAssessments(Id)
			--ON DELETE CASCADE
			ON DELETE SET NULL;

	-- Introducing FOREIGN KEY constraint 'FK_CoreAssessments_CoreAssessments' on table 'CoreAssessments' may cause cycles or multiple cascade paths. Specify ON DELETE NO ACTION or ON UPDATE NO ACTION, or modify other FOREIGN KEY constraints.
	-- . it doesn't work both in "On delete casacde and on delete set null"

/*


*/


-- ==============
-- OTHER INFO
-- ==============
/*
WITH CHECK
	- default is "WITH CHECK", if "WITH CHECK" then it will validate the current data with the new constraint. Don't use "WITH NO CHECK"
DBCC DROPCLEANBUFFERS
	- removes all clean buffers from the buffer pool, and columnstore objects from the colujmnstore object pool. "we added a "DBCC DROPCLEANBUFFERS" to each DML query to ensure there is no data in the cache before we run the statement."
	- https://www.mssqltips.com/sqlservertip/1360/clearing-cache-for-sql-server-performance-testing/
>>>>>>> Stashed changes
*/