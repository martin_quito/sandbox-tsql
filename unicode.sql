﻿CREATE TABLE #temp  (name NVARCHAR(32), lastname VARCHAR(32));

INSERT INTO #temp SELECT 'MARTIN', 'MARTIN'
INSERT INTO #temp SELECT 'طريقة اللعب', 'طريقة اللعب'
INSERT INTO #temp SELECT N'طريقة اللعب', N'طريقة اللعب'



SELECT *
FROM #temp

/*
name			lastname
----            --------
MARTIN			MARTIN
????? ?????		????? ?????
طريقة اللعب	????? ?????
*/

DROP TABLE #temp


/*
NVARCHAR 
	- 2 bytes per character, so "MARTIN", uses 12 bytes
	- stores unicode data, if you need to store unicode or multilingual data, nvarchar is your choice
	- need to use N to insert to insert other characters
VARCHAR 
	- 1 byte per character, so "MARTIN" uses 6 bytes
	- stores ASCII data nad should be your data type of chose for normal use


- since unicode is global now, use nvarchar always

*/

-- ======================
/*

Tab				char(9)
Line feed		char(10)
Carriage return	char(13)

*/