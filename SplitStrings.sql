-- =====================================
-- Splitty command separated values
-- =====================================
DECLARE @CourseOutcomes TABLE (CourseId INT, Outcome NVARCHAR(MAX))
INSERT INTO @CourseOutcomes VALUES
  (1, 'out1, out2, out3, out4')
, (2, 'out100, out200, out300')
, (3, 'out001, out002, out003')

;WITH tmp (CourseId, Outcome, Outcomes) AS
(
	SELECT co.CourseId, 
					convert( nvarchar(max),
					ltrim(rtrim(left(co.Outcome, charindex(',',co.Outcome+',')-1)))),
					convert(nvarchar(max), stuff(co.Outcome, 1, charindex(',',co.Outcome+','),''))
	FROM @CourseOutcomes co
	UNION ALL
	SELECT CourseId
			, convert(nvarchar(max),ltrim(rtrim(left(Outcomes, charindex(',',Outcomes+',')-1))))
			, convert(nvarchar(max), stuff(Outcomes, 1, charindeX(',',Outcomes+','),''))
	FROM tmp
	WHERE Outcomes > ''
)	SELECT *
	FROM tmp
	ORDER BY COurseId
