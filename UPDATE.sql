DECLARE @table1 TABLE (Id INT, NewOtherId INT)
INSERT INTO @table1 VALUES (1, NULL)

DECLARE @table2 TABLE (Id INT, OtherId INT)
INSERT INTO @table2 VALUES
(100, 1)
, (110, 1)

UPDATE t1
SET NewOtherId = t2.Id
FROM @table1 t1
	INNER JOIN @table2 t2 ON t1.Id = t2.OtherId


SELECT *
FROM @table1