-- ==================
-- removing duplicates Using WHERE EXISTS
-- ==================
/* returning only duplicates */
DECLARE @aTable1 TABLE (id INT, MyId INT, firstName NVARCHAR(MAX))
INSERT INTO @aTable1 VALUES
  (1, 1, 'Martin')
, (2, 1, 'Martin')
, (3, 1, 'Martin')
, (4, 2, 'Amber')
, (5, 2, 'Amber')
, (6, 3, 'Allison')
, (7, 3, 'Allison')

SELECT *
FROM @aTable1 a
WHERE  EXISTS (
	SELECT 1
	FROM @aTable1 b
	WHERE a.MyId = b.MyId	-- the duplicates values
	AND a.Id > b.Id			-- the primary value
)
	-- . This returns only the duplicates. The uniques (lowest) Id's don't get displayed 
	-- . EXISTS with "> from a to b (top to bottom). returns only the duplicates. 
	-- . Each record of A will get evaluated . When A record's ID is greater than B's record ID then show. 
	--   So because of comparing a.MyId = b.MyId, it creates a group of only the duplicates. For for the first group, it will 
	--   grab the first value of A and see if its Id is bigger than B's Id. The id's will be the same so is a FALSE. So that will not return
	--   the rest of the values will return because it receives at least one TRUE

/* returning only uniques */
DECLARE @aTable3 TABLE (id INT, MyId INT, firstName NVARCHAR(MAX))
INSERT INTO @aTable3 VALUES
  (1, 1, 'Martin')
, (2, 1, 'Martin')
, (3, 1, 'Martin')
, (4, 2, 'Amber')
, (5, 2, 'Amber')
, (6, 3, 'Allison')
, (7, 3, 'Allison')

SELECT *
FROM @aTable3 a
WHERE NOT EXISTS (
	SELECT 1
	FROM @aTable3 b
	WHERE a.MyId = b.MyId	-- the duplicates values
	AND a.Id > b.Id			-- the primary value
)
	-- . Returns uniques (lowest id's)
	
	-- how does the where work
	DECLARE @aTable2 TABLE (id INT, MyId INT, firstName NVARCHAR(MAX), Country NVARCHAR(MAX))
	INSERT INTO @aTable2 VALUES
	  (1, 1, 'Martin', 'Peru')
	, (2, 1, 'Martin', 'Peru')
	, (3, 1, 'Martin', 'Peru')
	, (4, 2, 'Amber', 'Peru')
	, (5, 2, 'Amber', 'Peru')
	, (6, 3, 'Allison', 'Peru')
	, (7, 3, 'Allison', 'Peru')
	SELECT *
	FROM @aTable2
	WHERE Country = 'Peru'
	AND MyId < 2


-- ======= remove duplicates ========
DECLARE @a TABLE (id INT, name NVARCHAR(MAX))
INSERT INTO @a VALUES 
  (1, 'Martin')
, (2, 'Martin')
, (3, 'Martin')

SELECT * 
FROM @a t
WHERE NOT EXISTS (SELECT 1
				  FROM @a d
				  WHERE t.id > d.id)	
		-- 2 > 1, and 3 > 1 | those are true which will be removed from the top query

-- =================
-- SET XACT_ABORT ON
-- =================
-- "Specifies whether SQL Server automatically rolls back the current transaction when a Transact-SQL statement raises a run-time error."