-- =====
-- give me all the brands that have more than one record in the shoe table and that in each brand, the sizes are different. If they are the same, don't return. 
-- =====
DECLARE @Shoes TABLE (Brand NVARCHAR(MAX), Size INT)
INSERT INTO @Shoes VALUES 
  ('Nike', 5)
, ('Nike', 6)
, ('Nike', 7)
, ('Adidas', 8)
, ('Adidas', 8)
, ('Puma', 5)
, ('Puma', 5)
, ('Joys', 4)
, ('Joys', 5)
, ('Vans', 9)
, ('MARTIN', 10) -- will not return
, ('Meow', 11) -- these two meows will not return because even though they are completely the same
, ('Meow', 11)

-- two or more records that are the same (based on what is specified) and are different on what is specified

-- grouped and join
;WITH grouped AS
(
	SELECT Brand, COUNT(Brand) AS 'Count'
	FROM @Shoes
	GROUP BY Brand
	HAVING COUNT(*) > 1
)	SELECT DISTINCT s.*
	FROM grouped g
		INNER JOIN @Shoes s ON g.Brand = s.Brand
		INNER JOIN @Shoes s2 ON s.Brand = s2.Brand
						AND s.Size != s2.Size

-- exist clause
SELECT *
FROM @Shoes s
WHERE EXISTS (
	SELECT *
	FROM @Shoes sh
	WHERE s.Brand = sh.Brand
	AND s.Size != sh.Size
)


