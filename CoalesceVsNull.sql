-- =============
-- coalesce
-- =============
/*
	Evaluates the arguments in order and returns the current value of the first expression that initially does not evaluate to NULL. In other words, it returns the first variable THAT IS NOT NULL or contains a value
*/

DECLARE @a INT = 1,
		@b INT = NULL,
		@c INT = 3;
SELECT COALESCE(@a, @b, @c)
	-- returns @a which is 1


DECLARE @d INT = NULL,
		@e INT = 2,
		@f INT = 3;
SELECT COALESCE(@d, @e, @f)
	-- returns @b which is 2


DECLARE @g INT = NULL,
		@h INT = NULL,
		@i INT = NULL;
SELECT COALESCE(@g, @h, @i)
	-- returns NULL


-- =============
-- IS NULL
-- =============
-- returns NULL within the specified replacement value
DECLARE @StudentName NVARCHAR(MAX) = NULL;
SELECT ISNULL(@StudentName, 'blah')
	-- blah
	-- . ISNULL (check_expression, replacement_value). If "check_expression" is null, then return that value ('blah'). If is not null then return the first argument.
	-- . if this is null then return this.

DECLARE @aa NVARCHAR(MAX) = 'martin'
SELECT ISNULL(@aa, 'Im the new value')	
	-- martin


/*
Difference IS_NULL vs COALESCE
	- coalease supports more than two arguments, is_null just 2
	- coalesce is a standard function (ISO/ANSI SQL) whereas ISNULL is T-SQL-specific
*/

-- =============
-- COALESCE in order
-- =============
DECLARE @table1 TABLE (Id INT, FirstName NVARCHAR(MAX))
INSERT INTO @table1 VALUES
(1, 'A')
, (2, 'B')
, (3, 'C')
, (4, NULL)
, (5, 'M')

SELECT *
FROM @table1
ORDER BY FirstName -- default is asce (from small to high)
	-- NULL
	-- A
	-- B
	-- C
	-- M
	-- . in order by, NULL is considered to be smaller than A

SELECT *
FROM @table1
ORDER BY COALESCE(FirstName, 'F')
	-- A
	-- B
	-- C
	-- NULL
	-- M
	-- . the coalesce helps to give a new value if the value is NULL!
