-- =============
--- NUMERIC and DECIMAL
-- =============
/*
Approximate Numerics
- FLOAT
- REAL

EXACT NUMERICS
- DECIMAL
- NUMERIC

	DECIMAL[(p [,s])] (same thing)
	NUMERIC[(p [,s])] (same thing)

	p (precision)
		- "the maximum total number of decimal digits that can be stored, both to the left and to the right of the decimal point. The
		- "precision must be a value from 1 through the maximum PRECISION Of 38. The default precision is 18
	s (scale)
		- "The maximum number of decimal digits that can be be stored to the right of the decimal point. Scale must be a value from 0 through p. Scale can be specified only if preciison is specified. The default scale is 0; therefore 0 <= s  <= p. 

	Precision and Scale
	(1-9)	| 5
	(10-19) | 9
	(20-28) | 13
	(29-38) | 17
*/

DECLARE @a NUMERIC(38, 37) = 2.123456789
SELECT @a
	-- 2.1234567890000000000000000000000000000
	-- . this means that it will enter zeros as trailing zeros

DECLARE @b NUMERIC(38, 2) = 2.199
SELECT @b
	-- rounds up because of the scale		

DECLARE @c NUMERIC(38) = 2.32321321
SELECT @C
	-- 2
	-- . it rounded up to a whole number because no scale

DECLARE @d NUMERIC (38,38) = 2.23
	-- "Arithmetic overflow error converting int to data type numeric"

DECLARE @e NUMERIC (38, 37) = 2333.23131
	-- it can't because the 37 makes it to have only 1 digit on the right of the decimal
-- ================
-- ROUND
-- ================
SELECT ROUND(748.58, 0)
	-- 749.00
	--. 

SELECT ROUND(748.58, 1)
	-- 748.60
	-- . 5 is rounded

SELECT ROUND(748.58, 2)
	-- 748.58
	-- . 8 is rounded but has nothing so no rounding happens

SELECT ROUND(748.58, -1)
	-- 750.00

SELECT ROUND(748.58, -2)
	-- 700.00

SELECT ROUND(117.24650000, 2)
	-- 117.25000000
	-- if cast it to a nuermic or decimal then those zeros are removed.

SELECT ROUND(117.2491, 2)
	-- 117.2500
	-- "24" is rounded and the rest becomes zeros!

