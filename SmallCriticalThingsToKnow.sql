-- ====================
-- WHERE Case or not case sensititive
-- ====================
SELECT 1
WHERE 'a' = 'A'
	-- not case sensitive by default

SELECT 1
WHERE 'A' = 'A             '
	-- it front trims spaces 

SELECT 1
WHERE 'A' =  '  A             '
	-- false





-- ==================
-- same scenraio as above but with joins
-- ==================

DECLARE @temp TABLE (
	Name1 NVARCHAR(MAX)
	, Name2 NVARCHAR(MAX)
)
INSERT INTO @temp VALUES ('Martin', 'Martin')
, ('Nefi', 'nefi')
, ('Miriam', ' Miriam         ')

SELECT t1.*, '===', t2.*
FROM @temp t1
	LEFT JOIN @temp t2 ON t1.Name1 = t2.Name2
